using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using System.Net;

namespace Bardar.Droid
{
    public class DetailFragment : Android.Support.V4.App.Fragment
    {
        private TextView header;
        private TextView rating;
        private TextView address;
        private TextView pricelist;
        private TextView openinghours;
        private ImageView imageView;
        private View myView;
        public static Android.Support.V4.App.Fragment newInstance(Context context)
        {
            DetailFragment busrouteFragment = new DetailFragment();
            return busrouteFragment;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here


        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            myView = inflater.Inflate(Resource.Layout.DetailLayout, container, false);

            header = myView.FindViewById<TextView>(Resource.Id.detail_header);
            rating = myView.FindViewById<TextView>(Resource.Id.detail_rating);
            address = myView.FindViewById<TextView>(Resource.Id.detail_address);
            pricelist = myView.FindViewById<TextView>(Resource.Id.detail_pricelist);
            openinghours = myView.FindViewById<TextView>(Resource.Id.detail_openinghours);
            imageView = myView.FindViewById<ImageView>(Resource.Id.detail_imageView);

            return myView;
        }

        public void SetInformation(MyBar bar)
        {
            header.Text = bar.name;
            rating.Text = "Rating: "+bar.rating+" %";
            address.Text = bar.contact.street +" "+ bar.contact.number + "\n" + bar.contact.ZIP + "\n" + bar.contact.city;

            try
            {
                pricelist.Text = GenerateBeverageList(bar);
            }
            catch (Exception)
            {
            }

            try
            {
                openinghours.Text = GenerateOpeningList(bar);
            }
            catch (Exception)
            {
            }

            try
            {
                imageView.SetImageBitmap(GetImageBitmapFromUrl(bar.imageUri.ToString()));

            }
            catch (Exception)
            {
            }

        }

        private string GenerateOpeningList(MyBar bar)
        {
            StringBuilder builder = new StringBuilder();
            foreach (string openingtime in bar.openingTimes)
            {
                builder.Append(openingtime+ "\n");
            }
            return builder.ToString();
        }

        private Bitmap GetImageBitmapFromUrl(string url)
        {
            Bitmap imageBitmap = null;

            using (var webClient = new WebClient())
            {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                }
            }

            return imageBitmap;
        }

        private string GenerateBeverageList(MyBar bar)
        {
            StringBuilder builder = new StringBuilder();
            foreach (MyBeverage beverage in bar.menu)
            {
                builder.Append(beverage.name+ ":   " + beverage.price+"\n");
            }
            return builder.ToString();
        }
    }
}