using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Bardar.Droid
{
    public class DetailNewsfeedFragment : Android.Support.V4.App.Fragment
    {
        private TextView header;
        private TextView barname;
        private TextView description;
        private TextView date;
        private View myView;
        public static Android.Support.V4.App.Fragment newInstance(Context context)
        {
            DetailNewsfeedFragment busrouteFragment = new DetailNewsfeedFragment();
            return busrouteFragment;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            myView = inflater.Inflate(Resource.Layout.DetailNewsfeedLayout, container, false);

            header = myView.FindViewById<TextView>(Resource.Id.newsfeeddetail_header);
            barname = myView.FindViewById<TextView>(Resource.Id.newsfeeddetail_barname);
            description = myView.FindViewById<TextView>(Resource.Id.newsfeeddetail_description);
            date = myView.FindViewById<TextView>(Resource.Id.newsfeeddetail_date);

            return myView;
        }

        public void SetInformation(MyEvent myevent)
        {
            header.Text = myevent.name;
            barname.Text = myevent.bar;
            description.Text = myevent.description;
            date.Text = myevent.date.ToShortDateString();
        }
    }
}