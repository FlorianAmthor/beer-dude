using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;

namespace Bardar.Droid
{
    public class ListFragment : Android.Support.V4.App.Fragment
    {
        private View myView;
        private RecyclerView myRecyclerView;
        private MyAdapter myAdapter;
        private RecyclerView.LayoutManager myLayoutManager;
        private List<MyBar> barList;
        private MainActivity myactivity;

        public static Android.Support.V4.App.Fragment newInstance(Context context)
        {          
            ListFragment busrouteFragment = new ListFragment();
            return busrouteFragment;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
            myactivity = (MainActivity)this.Activity;

        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            myView = inflater.Inflate(Resource.Layout.ListLayout, container, false);

            myRecyclerView = (RecyclerView)myView.FindViewById(Resource.Id.recycler_view);

            LoadBars();

            return myView;
        }

        public async void LoadBars()
        {
            MyHttpClient client = new MyHttpClient();
            barList = await client.GetMyBarListRequest();

            //Performance enhance
            myRecyclerView.HasFixedSize = true;


            myLayoutManager = new LinearLayoutManager(Activity);  
            myRecyclerView.SetLayoutManager(myLayoutManager);
            myAdapter = new MyAdapter(barList);
            myRecyclerView.SetAdapter(myAdapter);
            myAdapter.ItemClick += MyAdapter_ItemClick;
        }

        private void MyAdapter_ItemClick(object sender, int e)
        {           
            myactivity.ShowDetailFragment(barList.ElementAt(e));
        }

        public async void searchBars(string searchstring)
        {
            MyHttpClient client = new MyHttpClient();
            barList = await client.BarSearchRequest(searchstring);

            //Performance enhance
            myRecyclerView.HasFixedSize = true;

            myLayoutManager = new LinearLayoutManager(Activity);
            myRecyclerView.SetLayoutManager(myLayoutManager);
            myAdapter = new MyAdapter(barList);
            myRecyclerView.SetAdapter(myAdapter);
            myAdapter.ItemClick += MyAdapter_ItemClick;
        }
    }
}