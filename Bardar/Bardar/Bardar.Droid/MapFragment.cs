using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Locations;
using Java.Util;

namespace Bardar.Droid
{
    public class MapFragment : Android.Support.V4.App.Fragment , IOnMapReadyCallback
    {
        private View myView;
        private SupportMapFragment myMapFragment;
        private GoogleMap myMap;
        private List<MyBar> barList;
        private Geocoder geocoder;
        private LatLng latlng;
        private LatLng myPosition;
        private float zoom = 13;

        public static Android.Support.V4.App.Fragment newInstance(Context context)
        {
            MapFragment busrouteFragment = new MapFragment();
            return busrouteFragment;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            geocoder = new Geocoder(Context);
            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            myView = inflater.Inflate(Resource.Layout.MapLayout, container, false);

            myMapFragment = (SupportMapFragment)ChildFragmentManager.FindFragmentById(Resource.Id.map);

            myMapFragment.GetMapAsync(this);
            
            return myView;
        }

        void IOnMapReadyCallback.OnMapReady(GoogleMap googleMap)
        {
            myMap = googleMap;

        
            myMap.UiSettings.IndoorLevelPickerEnabled = true;
            myMap.BuildingsEnabled = true;
            myMap.TrafficEnabled = true;
            myMap.UiSettings.MapToolbarEnabled = true;
            myMap.MyLocationEnabled = true;
            myMap.UiSettings.MyLocationButtonEnabled = true;
            myMap.UiSettings.CompassEnabled = true;
            myMap.UiSettings.RotateGesturesEnabled = true;
            myMap.UiSettings.ScrollGesturesEnabled = true;
            myMap.UiSettings.ZoomControlsEnabled = true;
            myMap.UiSettings.ZoomGesturesEnabled = true;
            myMap.MyLocationChange += MyMap_MyLocationChange;
            myMap.CameraChange += MyMap_CameraChange;
            
            loadBars();
        }

        private void MyMap_CameraChange(object sender, GoogleMap.CameraChangeEventArgs e)
        {
            if (myPosition!=null)
            {
                zoom = e.Position.Zoom;
                latlng = new LatLng(e.Position.Target.Latitude, e.Position.Target.Longitude);
            }
            
        }

        private void MyMap_MyLocationChange(object sender, GoogleMap.MyLocationChangeEventArgs e)
        {
            myPosition = new LatLng(e.Location.Latitude,e.Location.Longitude);
            if (latlng==null)
            {
                latlng = myPosition;
            }
            myMap.MoveCamera(CameraUpdateFactory.NewLatLngZoom(latlng,zoom));
        }

        private async void loadBars()
        {
            MyHttpClient client = new MyHttpClient();
            barList = await client.GetMyBarListRequest();

            IList <Address> addresslist = new List<Address>();

            foreach (MyBar bar in barList)
            {
                MyContact c = bar.contact;
                foreach (var item in await geocoder.GetFromLocationNameAsync(c.street + " " + c.number + " " + c.ZIP + " " + c.city, 1))
                {
                    addresslist.Add(item);
                }             
            }
            foreach (Address address in addresslist)
            {
                var marker = new MarkerOptions();
                marker.SetPosition(new LatLng(address.Latitude,address.Longitude));

                myMap.AddMarker(marker);
            }
        }
    }
}