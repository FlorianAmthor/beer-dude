using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Support.V7.Widget;
using Android.Widget;
using Android.Graphics;
using System.Net;

namespace Bardar.Droid
{
    public class MyNewsfeedAdapter : RecyclerView.Adapter
    {
        List<MyEvent> eventList;
        // Event handler for item clicks:
        public event EventHandler<int> ItemClick;



        public MyNewsfeedAdapter(List<MyEvent> EventList)
        {
            eventList = EventList;
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            // Inflate the CardView for the photo:
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.NewsfeedListCardLayout, parent, false);

            // Create a ViewHolder to find and hold these view references, and 
            // register OnClick with the view holder:
            MyViewHolder vh = new MyViewHolder(itemView, OnClick);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var myevent = eventList.ElementAt(position);

            // Replace the contents of the view with that element
            var holder = viewHolder as MyViewHolder;
            holder.Caption.Text = myevent.name;
            holder.Barname.Text = myevent.bar;
            holder.Date.Text = myevent.date.ToShortDateString();
            try
            {
                // Unn�tig, da events keine bilder haben
                // holder.Image.SetImageBitmap(GetImageBitmapFromUrl(bar.imageUri.ToString()));

            }
            catch (Exception)
            {

            }
        }

        public override int ItemCount
        {
            get
            {
                return eventList.Count;
            }
        }


        // Raise an event when the item-click takes place:
        void OnClick(int position)
        {
            if (ItemClick != null)
                ItemClick(this, position);
        }

        private class MyViewHolder : RecyclerView.ViewHolder
        {
            public TextView Caption { get; set; }
            public TextView Barname { get; set; }
            public TextView Date { get; set; }



            public MyViewHolder(View itemView, Action<int> listener) : base(itemView)
            {
                // Locate and cache view references:

                Caption = itemView.FindViewById<TextView>(Resource.Id.eventcard_caption);
                Barname = itemView.FindViewById<TextView>(Resource.Id.eventcard_barname);
                Date = itemView.FindViewById<TextView>(Resource.Id.eventcard_date);

                // Detect user clicks on the item view and report which item
                // was clicked (by position) to the listener:
                itemView.Click += (sender, e) => listener(base.AdapterPosition);
            }

        }

    }

}