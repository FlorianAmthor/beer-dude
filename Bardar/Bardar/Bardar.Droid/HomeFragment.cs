using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using System.Text.RegularExpressions;

namespace Bardar.Droid
{
    public class HomeFragment : Android.Support.V4.App.Fragment
    {
        private View myView;
        private EditText email;
        private EditText password;
        private MainActivity MyActivity;
        private EditText pw1;
        private EditText pw2;
        private EditText mail;
        private TextView homeText;
        private Button loginbutton;
        private Button registerbutton;

        public static Android.Support.V4.App.Fragment newInstance(Context context)
        {
            HomeFragment busrouteFragment = new HomeFragment();
            return busrouteFragment;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
            MyActivity = (MainActivity)Activity;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            myView = inflater.Inflate(Resource.Layout.HomeLayout, container, false);

            homeText = myView.FindViewById<TextView>(Resource.Id.home_textView);

            loginbutton = myView.FindViewById<Button>(Resource.Id.button_login);
            loginbutton.Click += Login_Click;
            registerbutton = myView.FindViewById<Button>(Resource.Id.button_register);
            registerbutton.Click += Register_Click;

            testforlogin();

            return myView;
        }

        private void testforlogin()
        {
            if (APIConstant.userIsLoggedIn)
            {
                homeText.SetText("Willkommen\n"+MyActivity.currentUser.name+" !",TextView.BufferType.Normal);

                loginbutton.Visibility = ViewStates.Invisible;
                loginbutton.Activated = false;

                registerbutton.Visibility = ViewStates.Invisible;
                registerbutton.Activated = false;
            }
            else
            {
                homeText.SetText("Nicht eingeloggt.", TextView.BufferType.Normal);

                loginbutton.Visibility = ViewStates.Visible;
                loginbutton.Activated = true;

                registerbutton.Visibility = ViewStates.Visible;
                registerbutton.Activated = true;
            }
        }

        private void Register_Click(object sender, EventArgs e)
        {
            var registerDialog = (new AlertDialog.Builder(Context)).Create();
            registerDialog.SetTitle("Registrieren");
            var viewRD = LayoutInflater.From(Activity).Inflate(Resource.Layout.RegisterDialog, null);

            pw1 = viewRD.FindViewById<EditText>(Resource.Id.register_password);
            pw2 = viewRD.FindViewById<EditText>(Resource.Id.register_password2);
            mail = viewRD.FindViewById<EditText>(Resource.Id.register_email);

            registerDialog.SetView(viewRD);
            registerDialog.SetButton2("Registrieren", registerHandlerRegister);
            registerDialog.SetButton("Abbrechen", registerHandlerCancel);
            registerDialog.Show();
        }

        private void Login_Click(object sender, EventArgs e)
        {
            var loginDialog = (new AlertDialog.Builder(Context)).Create();
            loginDialog.SetTitle("Anmelden");
            var viewLD = LayoutInflater.From(Activity).Inflate(Resource.Layout.LoginDialog, null);

            password = viewLD.FindViewById<EditText>(Resource.Id.login_password);
            email = viewLD.FindViewById<EditText>(Resource.Id.login_email);

            loginDialog.SetView(viewLD);
            loginDialog.SetButton2("Anmelden", loginHandlerLogin);
            loginDialog.SetButton("Abbrechen", loginHandlerCancel);
            loginDialog.Show();
        }

        public async void login(string email, string password)
        {

            if (email == "" || password == "")
            {
                Toast.MakeText(Context,"Email oder Passwort d�rfen nicht leer sein.",ToastLength.Long).Show();
            }
            else
            {
                // Allgemeinen HttpClient Einsetzen!
                var statusCode = await MyActivity.MyClient.FetchLoginToken(email, password);

                if (statusCode == System.Net.HttpStatusCode.OK)
                {
                    // Allgemeinen HttpClient Einsetzen!
                    MyActivity.MyClient.DefaultRequestHeaders.Authorization = APIConstant.MyClientAuthenticationHeader;
                    // Anpassen auf MainActivity
                    MyActivity.currentUser = await MyActivity.MyClient.GetUserInfo();
                    APIConstant.userIsLoggedIn = true;
                    // Anpassen auf MainActivity
                    MyActivity.UpdateUserItem();
                    //currentUserImage.Source = currentUser.image;
                }
                else
                {
                    APIConstant.userIsLoggedIn = false;
                    // Anpassen Auf Android
                    Toast.MakeText(Context, "Log-In Daten falsch.", ToastLength.Long).Show();
                }
            }

        }
        void loginHandlerLogin(object sender, DialogClickEventArgs e)
        {
            login(email.Text, password.Text);
        }
        void loginHandlerCancel(object sender, DialogClickEventArgs e)
        {
            //Do Nothing
        }
        void registerHandlerRegister(object sender, DialogClickEventArgs e)
        {
            register(mail.Text,pw1.Text,pw2.Text);
        }
        void registerHandlerCancel(object sender, DialogClickEventArgs e)
        {
            //Do Nothing
        }

        public void forgotPassword()
        {
            // Navigate to Password Forgot Fragment and View
        }

        public async void register(string email, string password, string confirmPassword)
        {
            if (email == "" || password == "" || confirmPassword == "")
            {
                // Anpassen Auf Android
                Toast.MakeText(Context, "Felder d�rfen nicht leer sein.", ToastLength.Long).Show();
            }
            else
            {

                bool isEmail = Regex.IsMatch(email, @"\A(?:[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?)\Z");

                if (password == confirmPassword && isEmail)
                {
                    if (email != "")
                    {
                        // Allgemeinen HttpClient ersetzen
                        var statusCode = await MyActivity.MyClient.RegisterUser(email, password);
                        if (statusCode == System.Net.HttpStatusCode.OK)
                        {
                            // Auf Android anpassen
                            Toast.MakeText(Context, "Um deine Anmeldung abzuschlie�en, klicke bitte auf Verifizierungslink in deiner Email!", ToastLength.Long).Show();
                        }
                        else
                        {
                            // Auf Android anpassen
                            Toast.MakeText(Context, MyActivity.MyClient.HandleHttpCode(statusCode), ToastLength.Long).Show();
                        }
                        
                        //Show new homescreen

                    }
                }
                else
                {
                    if (!isEmail)
                    {
                        // Auf Android Anpassen
                        Toast.MakeText(Context, "Keine g�ltige E-mail Adresse.", ToastLength.Long).Show();
                    }
                    else
                    {
                        // Auf Android Anpassen
                        Toast.MakeText(Context, "Passw�rter stimmen nicht �berein.", ToastLength.Long).Show();
                    }
                }
            }
        }

        //public async void changePassword(string password, string confirmPassword, string changeCode)
        //{
        //    if (password == "" || confirmPassword == "")
        //    {
        //        // Auf Android anpassen
        //        MyActivity.ShowGeneralMessage("Passwort darf nicht leer sein!", MyActivity.NotifyType.ErrorMessage);
        //    }
        //    else if (NewPasswordBox.Password != NewPasswordConfirmBox.Password)
        //    {
        //        // Auf Android anpassen
        //        MyActivity.ShowGeneralMessage("Die Passw�rter stimmen nicht �berein!", MyActivity.NotifyType.ErrorMessage);
        //    }
        //    else if (true)
        //    {
        //        if (APIConstant.userIsLoggedIn)
        //        {
        //            // Allgemeinen HttpClient ersetzen
        //            var statusCode = await MyActivity.MyClient.ChangeUserPassword(NewPasswordBox.Password);

        //            if (statusCode == System.Net.HttpStatusCode.OK)
        //            {
        //                // Auf Android anpassen
        //                MyActivity.ShowGeneralMessage("Passwort wurde erfolgreich ge�ndert!", MyActivity.NotifyType.StatusMessage);
        //            }
        //            else
        //            {
        //                // Auf Android anpassen
        //                MyActivity.ShowGeneralMessage(MyActivity.MyClient.HandleHttpCode(statusCode), MyActivity.NotifyType.ErrorMessage);
        //            }
        //        }
        //        else
        //        {
        //            if (changeCode == "")
        //            {
        //                // Auf Android anpassen
        //                MyActivity.ShowGeneralMessage("Es muss ein Best�tigungscode angegeben werden!", MyActivity.NotifyType.StatusMessage);
        //            }
        //            else
        //            {
        //                // Allgemeinen HttpClient ersetzen
        //                var statusCode = await MyActivity.MyClient.ChangeForgottenUserPassword(forgottenPasswordEmail, NewPasswordBox.Password, ConfirmationCodeTextBox.Text);

        //                if (statusCode == System.Net.HttpStatusCode.OK)
        //                {
        //                    // Auf Android anpassen
        //                    MyActivity.ShowGeneralMessage("Passwort wurde erfolgreich ge�ndert!", MyActivity.NotifyType.StatusMessage);
        //                }
        //                else
        //                {
        //                    // Auf android anpassen
        //                    MyActivity.ShowGeneralMessage(MyActivity.MyClient.HandleHttpCode(statusCode), MyActivity.NotifyType.ErrorMessage);
        //                }
        //            }
        //        }
        //    }
        //}
    }
}