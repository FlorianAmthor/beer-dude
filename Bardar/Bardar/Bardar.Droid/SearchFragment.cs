using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.Design.Widget;

using SupportFragment = Android.Support.V4.App.Fragment;

namespace Bardar.Droid
{
    public class SearchFragment : Android.Support.V4.App.Fragment
    {
        private View myFragmentView;
        private TabLayout sTabs;
        private SupportFragment mCurrentFragment = new SupportFragment();
        private ListFragment listFragment;
        private MapFragment mapFragment;

        public static SupportFragment newInstance(Context context)
        {
            SearchFragment busrouteFragment = new SearchFragment();
            return busrouteFragment;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);

            // Create your fragment here

        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            myFragmentView = inflater.Inflate(Resource.Layout.SearchLayout, container, false);

            sTabs = myFragmentView.FindViewById<TabLayout>(Resource.Id.tabs);

            TabLayout.Tab listTab = sTabs.NewTab().SetText(Resource.String.listTab);
            TabLayout.Tab mapTab = sTabs.NewTab().SetText(Resource.String.mapTab);

            listFragment = new ListFragment();
            mapFragment = new MapFragment();

            Android.Support.V4.App.FragmentTransaction tx = ChildFragmentManager.BeginTransaction();

            tx.Add(Resource.Id.tabcontent, listFragment);
            tx.Add(Resource.Id.tabcontent, mapFragment);
            tx.Hide(mapFragment);

            tx.Show(listFragment);

            tx.Commit();

            mCurrentFragment = listFragment;

            sTabs.AddTab(listTab);
            sTabs.AddTab(mapTab);
            sTabs.TabSelected += Tab_Selected;         

            return myFragmentView;
        }

        private void ShowFragment(SupportFragment fragment)
        {

            if (fragment.IsVisible)
            {
                return;
            }

            var trans = ChildFragmentManager.BeginTransaction();


            fragment.View.BringToFront();
            mCurrentFragment.View.BringToFront();

            trans.Hide(mCurrentFragment);
            trans.Show(fragment);

            trans.AddToBackStack(null);
            trans.Commit();

            mCurrentFragment = fragment;

        }

        public void SearchListPassThrough(string searchstring)
        {
            ShowFragment(listFragment);
            listFragment.searchBars(searchstring);
        }

        public void BarListPassThrough()
        {
            ShowFragment(listFragment);
            listFragment.LoadBars();
        }

        void Tab_Selected(object sender, TabLayout.TabSelectedEventArgs e)
        {
            //Android.Support.V4.App.Fragment fragment = null;

            switch (e.Tab.Position)
            {
                case 0:
                    ShowFragment(listFragment);
                    break;
                case 1:
                    ShowFragment(mapFragment);
                    break;
                
            }
        }
    }
}