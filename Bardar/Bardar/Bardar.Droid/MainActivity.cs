﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using SupportFragment = Android.Support.V4.App.Fragment;
using SupportSearchView = Android.Support.V7.Widget.SearchView;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using System.Collections.Generic;
using Android.Support.Design.Widget;
using Android.Support.V4.View;

namespace Bardar.Droid
{
	[Activity (Label = "Beer Dude", MainLauncher = false)]
	public class MainActivity : AppCompatActivity
	{
        private SupportToolbar mToolbar;
        private DrawerLayout mDrawerLayout;
        private ActionBarDrawerToggle mDrawerToggle;
        private HomeFragment homeFragment;
        private SearchFragment searchFragment;
        private NewsfeedFragment newsfeedFragment;
        private OptionFragment optionFragment;
        private DetailFragment detailFragment;
        private DetailNewsfeedFragment detailNewsfeedFragment;
        private NavigationView mNavigationView;
        private SupportFragment mCurrentFragment = new SupportFragment();
        private SupportSearchView searchView;

        public MyHttpClient MyClient;
        public MyUser currentUser;
  
 protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            MyClient = new MyHttpClient();
            SetContentView(Resource.Layout.Main);

            mToolbar = FindViewById < SupportToolbar > (Resource.Id.toolbar);
            mDrawerLayout = FindViewById < DrawerLayout > (Resource.Id.drawer_layout);
            mNavigationView = FindViewById<NavigationView>(Resource.Id.nav_view);

            mNavigationView.NavigationItemSelected += MenuListView_ItemClick;

            homeFragment = new HomeFragment();
            searchFragment = new SearchFragment();
            newsfeedFragment = new NewsfeedFragment();
            optionFragment = new OptionFragment();
            detailFragment = new DetailFragment();
            detailNewsfeedFragment = new DetailNewsfeedFragment();

            SetSupportActionBar(mToolbar);

            //Enable support action bar to display hamburger
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            SupportActionBar.SetTitle(Resource.String.homeTitle);

            mDrawerToggle = new ActionBarDrawerToggle(
            this, //Host Activity
            mDrawerLayout,//DrawerLayout
            mToolbar,//Toolbar
            Resource.String.openDrawer, //Opened Message
            Resource.String.closeDrawer //Closed Message
            );

            mDrawerLayout.AddDrawerListener(mDrawerToggle);
            mDrawerToggle.SyncState();

            Android.Support.V4.App.FragmentTransaction tx = SupportFragmentManager.BeginTransaction();

            tx.Add(Resource.Id.main, homeFragment);
            tx.Add(Resource.Id.main, searchFragment);
            tx.Add(Resource.Id.main, newsfeedFragment);
            tx.Add(Resource.Id.main, optionFragment);
            tx.Add(Resource.Id.main, detailFragment);
            tx.Add(Resource.Id.main, detailNewsfeedFragment);          
            tx.Hide(searchFragment);
            tx.Hide(newsfeedFragment);
            tx.Hide(optionFragment);
            tx.Hide(detailFragment);
            tx.Hide(detailNewsfeedFragment);

            mCurrentFragment = homeFragment;
            tx.Commit();
        }
        void MenuListView_ItemClick(object sender, NavigationView.NavigationItemSelectedEventArgs e)
        {
            //Android.Support.V4.App.Fragment fragment = null;

            switch (e.MenuItem.ItemId)
            {
                case Resource.Id.nav_home:
                    ShowFragment(homeFragment);
                    SupportActionBar.SetTitle(Resource.String.home);
                    break;
                case Resource.Id.nav_search:                  
                    ShowFragment(searchFragment);
                    searchFragment.BarListPassThrough();
                    SupportActionBar.SetTitle(Resource.String.barlist);
                    break;
                case Resource.Id.nav_newsfeed:
                    ShowFragment(newsfeedFragment);
                    SupportActionBar.SetTitle(Resource.String.newsfeed);
                    break;
                case Resource.Id.nav_settings:
                    ShowFragment(optionFragment);
                    SupportActionBar.SetTitle(Resource.String.settings);
                    break;
            }

            //SupportFragmentManager.BeginTransaction().Replace(Resource.Id.main, fragment).Commit();


            mDrawerLayout.CloseDrawers();
            mDrawerToggle.SyncState();

        }
        private void ShowFragment(SupportFragment fragment)
        {

            if (fragment.IsVisible)
            {
                return;
            }

            var trans = SupportFragmentManager.BeginTransaction();


            fragment.View.BringToFront();
            mCurrentFragment.View.BringToFront();

            trans.Hide(mCurrentFragment);
            trans.Show(fragment);

            trans.AddToBackStack(null);
            trans.Commit();

            mCurrentFragment = fragment;

        }
        public void ShowDetailFragment(MyBar bar)
        {
            ShowFragment(detailFragment);
            detailFragment.SetInformation(bar);
            SupportActionBar.SetTitle(Resource.String.bardetail);
        }

        public void ShowDetailNewsfeedFragment(MyEvent myevent)
        {
            ShowFragment(detailNewsfeedFragment);
            detailNewsfeedFragment.SetInformation(myevent);
            SupportActionBar.SetTitle(Resource.String.newsdetail);
        }

        public void UpdateUserItem()
        {
            if (currentUser.name == null)
            {
                currentUser.name = "Noch nicht gesetzt!";
            }
            //currentUserName.Text = currentUser.name;
            //currentUserEmail.Text = currentUser.email;
            //currentUserImage.Source = currentUser.image
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    mDrawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        protected override void OnPostCreate(Bundle savedInstanceState)
        {
            base.OnPostCreate(savedInstanceState);
            mDrawerToggle.SyncState();
        }

	    public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.nav_search, menu);

            var item = menu.FindItem(Resource.Id.action_search);

            var searchItem = item.ActionView;
            searchView = searchItem.JavaCast<SupportSearchView>();

            searchView.QueryTextSubmit += (s, e) =>
            {
                ShowFragment(searchFragment);             
                searchFragment.SearchListPassThrough(e.Query);
            };

            return true;
        }
    }
}


