using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Support.V7.Widget;
using Android.Widget;
using Android.Graphics;
using System.Net;

namespace Bardar.Droid
{
    public class MyAdapter : RecyclerView.Adapter
    {
        private List<MyBar> barList;
        // Event handler for item clicks:
        public event EventHandler<int> ItemClick;



        public MyAdapter(List<MyBar> BarList)
        {
            barList = BarList;
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            // Inflate the CardView for the photo:
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.ListCardLayout, parent, false);

            // Create a ViewHolder to find and hold these view references, and 
            // register OnClick with the view holder:
            MyViewHolder vh = new MyViewHolder(itemView, OnClick);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var bar = barList.ElementAt(position);

            // Replace the contents of the view with that element
            var holder = viewHolder as MyViewHolder;
            holder.Caption.Text = bar.name;
            holder.Rating.Text = "Rating: " +bar.rating + " %";
            try
            {
                holder.Image.SetImageBitmap(GetImageBitmapFromUrl(bar.imageUri.ToString()));

            }
            catch (Exception)
            {
                
            }
        }

        public override int ItemCount
        {
            get
            {
                return barList.Count;
            }
        }

        private Bitmap GetImageBitmapFromUrl(string url)
        {
            Bitmap imageBitmap = null;

            using (var webClient = new WebClient())
            {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                }
            }

            return imageBitmap;
        }

        // Raise an event when the item-click takes place:
        void OnClick(int position)
        {
            ItemClick?.Invoke(this, position);
        }

        private class MyViewHolder : RecyclerView.ViewHolder
        {
            public ImageView Image { get; set; }
            public TextView Caption { get; set; }
            public TextView Rating { get; set; }


            public MyViewHolder(View itemView, Action<int> listener) : base(itemView)
            {
                // Locate and cache view references:
                Image = itemView.FindViewById<ImageView>(Resource.Id.card_imageView);
                Caption = itemView.FindViewById<TextView>(Resource.Id.card_captionView);
                Rating = itemView.FindViewById<TextView>(Resource.Id.card_ratingView);

                // Detect user clicks on the item view and report which item
                // was clicked (by position) to the listener:
                itemView.Click += (sender, e) => listener(base.AdapterPosition);
            }

        }

    }

}