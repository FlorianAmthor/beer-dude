using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;

namespace Bardar.Droid
{
    public class NewsfeedFragment : Android.Support.V4.App.Fragment
    {
        private View myView;
        private RecyclerView myRecyclerView;
        private MyNewsfeedAdapter myAdapter;
        private RecyclerView.LayoutManager myLayoutManager;
        private List<MyEvent> eventList;
        private MainActivity myactivity;
        public static Android.Support.V4.App.Fragment newInstance(Context context)
        {
            NewsfeedFragment busrouteFragment = new NewsfeedFragment();
            return busrouteFragment;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
            myactivity = (MainActivity)this.Activity;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            myView = inflater.Inflate(Resource.Layout.NewsfeedListLayout, container, false);

            myRecyclerView = (RecyclerView)myView.FindViewById(Resource.Id.feedrecycler_view);
            LoadEvents();

            return myView;
        }

        private async void LoadEvents()
        {
            MyHttpClient client = new MyHttpClient();
            eventList = await client.GetMyEventListRequest();

            //Performance enhance
            myRecyclerView.HasFixedSize = true;


            myLayoutManager = new LinearLayoutManager(Activity);
            myRecyclerView.SetLayoutManager(myLayoutManager);
            myAdapter = new MyNewsfeedAdapter(eventList);
            myRecyclerView.SetAdapter(myAdapter);
            myAdapter.ItemClick += MyAdapter_ItemClick;
        }

        private void MyAdapter_ItemClick(object sender, int e)
        {
            myactivity.ShowDetailNewsfeedFragment(eventList.ElementAt(e));
        }
    }
}