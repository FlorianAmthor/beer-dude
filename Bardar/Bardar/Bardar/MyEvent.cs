﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bardar
{
    public class MyEvent
    {
        //<Attributes>

        //unique event id for MongoDB
        public string _id { get; set; }

        //name of the event
        public string name { get; set; }

        //description of the event, e.g.: all cocktails for half the price
        public string description { get; set; }

        //date and time when the event takes place
        public DateTime date { get; set; }

        //name of the bar where the event takes place
        public string bar { get; set; }

        //id of the bar where the event takes place
        public string barId { get; set; }

        //</Attributes>

        public MyEvent()
        {

        }
    }
}
