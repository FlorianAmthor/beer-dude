﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security;
using System.Security.Principal;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Bardar
{
    public class MyHttpClient : HttpClient
    {
        public MyHttpClient()
        {
            BaseAddress = new Uri(APIConstant.baseaddress);
            DefaultRequestHeaders.Accept.Clear();
            DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public bool IsUserLoggedIn()
        {
            return APIConstant.userIsLoggedIn;
        }

        public async void getTest()
        {
            var result = await GetAsync(BaseAddress);
        }

        //send a POST request to the MongoDB server and returns the responseMessage
        private async Task<HttpResponseMessage> SendJsonPostAsync(string ApiAddress, string data)
        {
            StringContent httpContent = new StringContent(data, Encoding.UTF8, "application/json");

            var responseMessage = await PostAsync(ApiAddress, httpContent);

            return responseMessage;
        }



        //send a GETST request to the MongoDB server and returns the responseMessage
        private async Task<HttpResponseMessage> SendJsonGetAsync(string ApiAddress)
        {
            var responseMessage = await GetAsync(ApiAddress);
            return responseMessage;
        }

        //logs in to the MongoDB Server and adds the received token to the HttpClient Authorization header
        public async Task<HttpStatusCode> FetchLoginToken(string Username, string Password)
        {
            var login = new JObject {{"email", Username}, {"password", Password}};


            var response = await SendJsonPostAsync(APIConstant.login, login.ToString());
            var loginToken = await response.Content.ReadAsStringAsync();
            login = JObject.Parse(loginToken);

            try
            {
                APIConstant.MyClientAuthenticationHeader = AuthenticationHeaderValue.Parse(("Bearer " + login["token"].ToString()));
            }
            catch (Exception)
            {
                // ignored
            }

            return response.StatusCode;
        }

        //sends username and password to MongoDB server and adds the user to the database
        public async Task<HttpStatusCode> RegisterUser(string Username, string Password)
        {
            JObject register = new JObject {{"email", Username}, {"password", Password}};


            var response = await SendJsonPostAsync(APIConstant.register, register.ToString());
            var loginToken = await response.Content.ReadAsStringAsync();
            register = JObject.Parse(loginToken);

            try
            {
                APIConstant.MyClientAuthenticationHeader = AuthenticationHeaderValue.Parse(("Bearer " + register["token"].ToString()));
            }
            catch (Exception)
            {
                // ignored
            }

            return response.StatusCode;
        }

        //change forgotten password
        public async Task<HttpStatusCode> ChangeForgottenUserPassword(string email, string newPassword, string confirmationCode)
        {
            JObject changePassword = new JObject
            {
                {"email", email}, {"password", newPassword}, {"code", confirmationCode}
            };


            var response = await SendJsonPostAsync(APIConstant.changeForgottenPassword, changePassword.ToString());

            return response.StatusCode;
        }

        //send password code to email
        public async Task<HttpStatusCode> SendPasswordCodeToEmail(string email)
        {
            JObject jObj = new JObject {{"email", email}};


            var response = await SendJsonPostAsync(APIConstant.sendPasswordCodeToEmail, jObj.ToString());

            return response.StatusCode;
        }

        //gets a list of bars depending on search parameter
        public async Task<List<MyBar>> BarSearchRequest(string searchString)
        {
            JObject searchObj = new JObject {{"search", searchString}};

            var response = await SendJsonPostAsync(APIConstant.barSearchRequest, searchObj.ToString());
            var jBarString = await response.Content.ReadAsStringAsync();
            var jBarArray = JArray.Parse(jBarString);

            var myBarList = jBarArray.ToObject<List<MyBar>>().ToList();

            return myBarList;
        }

        /* public async Task<HttpStatusCode> facebookLogin(string token)
         {
             JObject facebookToken = new JObject();
             facebookToken.Add("token", token);
             HttpResponseMessage response = await this.SendJsonPostAsync(APIConstant.facebookRegister, facebookToken.ToString());
             JObject register = JObject.Parse(response.ToString());
             try
             {
                 APIConstant.MyClientAuthenticationHeader = AuthenticationHeaderValue.Parse(("Bearer " + register["token"].ToString()));
             }
             catch (Exception)
             {

             }
             return response.StatusCode;
         }*/

        //gets a list of events depending on search parameter
        public async Task<List<MyEvent>> EventSearchRequest(string searchString)
        {
            JObject searchObj = new JObject {{"search", searchString}};

            var response = await SendJsonPostAsync(APIConstant.eventSearchRequest, searchObj.ToString());
            var jEventString = await response.Content.ReadAsStringAsync();
            var jEventArray = JArray.Parse(jEventString);

            var myEventList = jEventArray.ToObject<List<MyEvent>>().ToList();

            return myEventList;
        }

        //Gets the list of available bars by id
        public async Task<List<MyBar>> GetMyBarListByIdRequest(List<string> list)
        {
            var request = new JObject();
            var requestArr = new JArray();

            foreach (var item in list)
            {
                requestArr.Add(item);
            }

            request.Add("_ids", requestArr);

            var response = await SendJsonPostAsync(APIConstant.getBarsById, request.ToString());
            var jBarString = await response.Content.ReadAsStringAsync();
            requestArr = JArray.Parse(jBarString);

            var myBarList = requestArr.ToObject<List<MyBar>>().ToList();

            return myBarList;
        }

        //Gets the list of available bars
        public async Task<List<MyBar>> GetMyBarListRequest()
        {
            var response = await SendJsonGetAsync(APIConstant.getBars);
            var jBarString = await response.Content.ReadAsStringAsync();
            var jBarArray = JArray.Parse(jBarString);

            var myBarList = jBarArray.ToObject<List<MyBar>>().ToList();

            return myBarList;
        }

        //gets a specific bar by id
        public async Task<MyBar> GetMyBarRequest(string barId)
        {
            JObject jObj = new JObject();
            JObject jar = new JObject();

            jObj.Add("_id", barId);
            var response = await SendJsonPostAsync(APIConstant.getBarInfo, jObj.ToString());
            var jBarString = await response.Content.ReadAsStringAsync();

            jObj = JObject.Parse(jBarString);

            var barObj = jObj.ToObject<MyBar>();

            return barObj;
        }

        //Gets the list of available events
        public async Task<List<MyEvent>> GetMyEventListRequest()
        {
            var response = await SendJsonGetAsync(APIConstant.getEvents);
            var jEventString = await response.Content.ReadAsStringAsync();
            var jEventArray = JArray.Parse(jEventString);

            var myEventList = jEventArray.ToObject<List<MyEvent>>().ToList();

            return myEventList;
        }

        //gets a specific event by the eventId
        public async Task<MyEvent> GetMyEventRequest(string eventId)
        {
            var jObj = new JObject {{"_id", eventId}};

            var response = await SendJsonPostAsync(APIConstant.getEventInfo, jObj.ToString());
            var jEventString = await response.Content.ReadAsStringAsync();

            jObj = JObject.Parse(jEventString);
            var eventObj = jObj.ToObject<MyEvent>();

            return eventObj;
        }

        //gets a specific event by the barId
        public async Task<List<MyEvent>> GetMyEventListRequestForBar(string barId)
        {
            var jObj = new JObject {{"bar", barId}};

            var response = await SendJsonPostAsync(APIConstant.getEventsForBar, jObj.ToString());
            var jEventString = await response.Content.ReadAsStringAsync();

            var jArr = JArray.Parse(jEventString);
            var eventObjList = jArr.ToObject<List<MyEvent>>().ToList();

            return eventObjList;
        }

        //send a rating to the database
        public async Task<KeyValuePair<HttpStatusCode, int>> RateBar(string barId, int newRating)
        {
            KeyValuePair<HttpStatusCode, int> codeRating;
            var request = new JObject {{"_id", barId}, {"rating", newRating}};


            var response = await SendJsonPostAsync(APIConstant.rateBar, request.ToString());
            if (HttpStatusCode.OK == response.StatusCode)
            {
                var jString = await response.Content.ReadAsStringAsync();
                request = JObject.Parse(jString);
                var overallRating = (int)request["rating"];
                codeRating = new KeyValuePair<HttpStatusCode, int>(response.StatusCode, overallRating);
            }
            else
            {
                codeRating = new KeyValuePair<HttpStatusCode, int>(response.StatusCode, 0);
            }

            return codeRating;
        }

        public string HandleHttpCode(HttpStatusCode errCode)
        {
            string errString = "1";

            switch (errCode)
            {
                case HttpStatusCode.Accepted: //202
                    break;
                case HttpStatusCode.Ambiguous: //300
                    break;
                case HttpStatusCode.BadGateway: //502
                    break;
                case HttpStatusCode.BadRequest: //400
                    break;
                case HttpStatusCode.Conflict: //409
                    break;
                case HttpStatusCode.Continue: //100
                    break;
                case HttpStatusCode.Created: //201
                    break;
                case HttpStatusCode.ExpectationFailed: //417
                    break;
                case HttpStatusCode.Forbidden: //403
                    break;
                case HttpStatusCode.Found: //302
                    break;
                case HttpStatusCode.GatewayTimeout: //504
                    break;
                case HttpStatusCode.Gone: //410
                    break;
                case HttpStatusCode.HttpVersionNotSupported: //505
                    break;
                case HttpStatusCode.InternalServerError: // 500
                    errString = $"Da ist wohl was schief gegangen! Fehler: {errCode}";
                    break;
                case HttpStatusCode.LengthRequired: //411
                    break;
                case HttpStatusCode.MethodNotAllowed: //405
                    break;
                case HttpStatusCode.Moved: //301
                    break;
                case HttpStatusCode.NoContent: //204
                    break;
                case HttpStatusCode.NonAuthoritativeInformation: //203
                    break;
                case HttpStatusCode.NotAcceptable: //406
                    break;
                case HttpStatusCode.NotFound: //404
                    errString = $"Inhalt wurde nicht gefunden! Fehler: {errCode}";
                    break;
                case HttpStatusCode.NotImplemented: //501
                    break;
                case HttpStatusCode.NotModified: //304
                    break;
                case HttpStatusCode.OK: //200
                    errString = "Erfolg!";
                    break;
                case HttpStatusCode.PartialContent: //206
                    break;
                case HttpStatusCode.PaymentRequired: //402
                    break;
                case HttpStatusCode.PreconditionFailed: //412
                    break;
                case HttpStatusCode.ProxyAuthenticationRequired: //407
                    break;
                case HttpStatusCode.RedirectKeepVerb: //307
                    break;
                case HttpStatusCode.RedirectMethod: //303
                    break;
                case HttpStatusCode.RequestedRangeNotSatisfiable: //416
                    break;
                case HttpStatusCode.RequestEntityTooLarge: //413
                    break;
                case HttpStatusCode.RequestTimeout: //408
                    break;
                case HttpStatusCode.RequestUriTooLong: //414
                    break;
                case HttpStatusCode.ResetContent: //205
                    break;
                case HttpStatusCode.ServiceUnavailable: //503
                    break;
                case HttpStatusCode.SwitchingProtocols: //101
                    break;
                case HttpStatusCode.Unauthorized: //401
                    errString = "Email Adresse oder Passwort falsch! Bitte erneut versuchen";
                    break;
                case HttpStatusCode.UnsupportedMediaType: //415
                    break;
                case HttpStatusCode.Unused: //306
                    break;
                case HttpStatusCode.UpgradeRequired: //426
                    break;
                case HttpStatusCode.UseProxy: //305
                    break;
                default:
                    break;
            }
            return errString;
        }
        public async Task<HttpStatusCode> ChangeUserPassword(string newPassword)
        {
            var changePassword = new JObject {{"password", newPassword}};


            var response = await SendJsonPostAsync(APIConstant.changePassword, changePassword.ToString());

            return response.StatusCode;
        }


        //update user information
        public async Task<HttpStatusCode> UpdateUserInfo(string currentEmail, string newName, string newEmail)
        {
            var update = new JObject();
            var updateChangeset = new JObject();

            if (newName != "")
            {
                updateChangeset.Add("name", newName);
            }
            if (newEmail != "")
            {
                updateChangeset.Add("email", newEmail);
            }

            update.Add("email", currentEmail);
            update.Add("changeSet", updateChangeset);

            var response = await SendJsonPostAsync(APIConstant.updateUserInfo, update.ToString());

            return response.StatusCode;
        }

        //delete user account
        public async Task<HttpStatusCode> DeleteUserAccount()
        {
            try
            {
                var response = await SendJsonGetAsync(APIConstant.deleteUserAccount);
                return response.StatusCode;
            }
            catch (Exception)
            {
                return HttpStatusCode.Unauthorized;
            }
        }

        //update favourites
        public async Task<HttpStatusCode> UpdateFavourites(List<string> favs)
        {
            JObject update = new JObject();
            JArray updateArr = new JArray();

            updateArr.Add(favs);
            update.Add("favourites", updateArr);

            var response = await SendJsonPostAsync(APIConstant.updateFavourites, update.ToString());

            return response.StatusCode;
        }

        public async Task<List<MyBar>> GetFavourites()
        {
            var response = await SendJsonGetAsync(APIConstant.getFavourites);
            var jBarString = await response.Content.ReadAsStringAsync();
            var jBarArray = JArray.Parse(jBarString);

            var barList = jBarArray.ToObject<List<MyBar>>().ToList();

            return barList;
        }

        public async Task<List<MyEvent>> GetFavouriteEvents(List<string> idList)
        {
            var eventObject = new JObject();
            var eventArray = new JArray {idList};

            eventObject.Add("bars", eventArray);

            var response = await SendJsonPostAsync(APIConstant.getUserNewsFeed, eventObject.ToString());
            var jEventString = await response.Content.ReadAsStringAsync();
            eventArray = JArray.Parse(jEventString);

            var eventList = eventArray.ToObject<List<MyEvent>>().ToList();

            return eventList;
        }

        //update bar menu
        public async Task<HttpStatusCode> UpdateBarMenu(string barId, MyBeverage beverage)
        {
            var update = new JObject();

            var item = JObject.FromObject(beverage);
            update.Add("_id", barId);
            update.Add("item", item);

            var response = await SendJsonPostAsync(APIConstant.updateBarMenu, update.ToString());

            return response.StatusCode;
        }


        //get user information by email
        public async Task<MyUser> GetUserInfo()
        {
            var response = await SendJsonGetAsync(APIConstant.getUserInfo);
            var jUserString = await response.Content.ReadAsStringAsync();
            var jObj = JObject.Parse(jUserString);

            var userObj = jObj.ToObject<MyUser>();

            return userObj;
        }

        //get user information by email
        public async Task<HttpStatusCode> SendReport(string reportType, string email, string subject, string message)
        {
            var jReport = new JObject
            {
                {"reportType", reportType}, {"from", email}, {"subject", subject}, {"msg", message}
            };

            var response = await SendJsonPostAsync(APIConstant.sendReport, jReport.ToString());

            return response.StatusCode;
        }

        //Checks the HttpClient owns a token
        public bool IsLoginTokenAvailable()
        {
            return DefaultRequestHeaders.Authorization != null;
        }
    }
}