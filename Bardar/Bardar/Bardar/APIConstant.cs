﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Bardar
{
    public static class APIConstant
    {
        //Global Strings for MongoDB REST calls

        //Login API call
        public static string login { get { return "/auth/login"; } }

        //Login API call
        public static string register { get { return "/auth/register"; } }

        //GetBars API call
        public static string getBars { get { return "/api/open/bar/getBars"; } }

        //public static string facebookRegister { get { return "/auth/facebookRegister"; } }

        //getBars by ID API call
        public static string getBarsById { get { return "/api/open/bar/getBarList"; } }

        //rate Bar
        public static string rateBar { get { return "/api/bar/rateBar"; } }

        //get infos for a single bar API call
        public static string getBarInfo { get { return "/api/open/bar/getBarInfo"; } }

        //get list of bars depending on search parameter
        public static string barSearchRequest { get { return "/api/open/bar/searchRequest"; } }

        //get list of events depending on search parameter
        public static string eventSearchRequest { get { return "/api/open/event/searchRequest"; } }

        //update the bar menue with a new beverage
        public static string updateBarMenu { get { return "/api/bar/updateBarMenu"; } }

        //GetEvents API call
        public static string getEvents { get { return "/api/open/event/getEvents"; } }

        //get infos for a single Event API call
        public static string getEventInfo { get { return "/api/open/event/getEventInfo"; } }

        //get infos for a single Event in a bar API call
        public static string getEventsForBar { get { return "/api/open/event/getEventsForBar"; } }

        //update user information
        public static string updateUserInfo { get { return "/api/user/updateUserInfo"; } }

        //delete account API
        public static string deleteUserAccount { get { return "/api/user/deleteAccount"; } }

        //update favourites API
        public static string updateFavourites { get { return "/api/user/updateFavourites"; } }

        //get favourites API
        public static string getFavourites { get { return "/api/user/getFavourites"; } }

        //get favourite events from user
        public static string getUserNewsFeed { get { return "/api/open/event/getUserNewsFeed"; } }

        //change user password
        public static string changePassword { get { return "/api/user/changePassword"; } }

        //change forgotten user password
        public static string changeForgottenPassword { get { return "/auth/changeForgottenPassword"; } }

        //get user info by email
        public static string getUserInfo { get { return "/api/user/getUserInfo"; } }

        //send password code to email
        public static string sendPasswordCodeToEmail { get { return "/auth/sendPasswordCodeToEmail"; } }

        //send report
        public static string sendReport { get { return "/auth/sendReport"; } }
        //Header for HttpClient with token
        public static AuthenticationHeaderValue MyClientAuthenticationHeader { get; set;}

        //Bool that states if a user is already logged in
        public static bool userIsLoggedIn = false;

        //states if cached UserData has ben loaded
        public static bool userDataHasBeenLoad = false;

        //Server Baseaddress
        public static string baseaddress = "https://beerdude.de:8081";
    }
}
