﻿using System.Collections.Generic;

namespace Bardar
{
    public class MyUser
    {
        //<Attributes>

        //unique user id for MongoDB
        public string _id { get; set; }

        //profile name of the user
        public string name { get; set; }

        //email address of the user
        public string email { get; set; }

        //list of favorite bars
        public List<MyBar> favourites { get; set; }

        //</Attributes>
        public MyUser()
        {
            favourites = new List<MyBar>();
        }
    }
}

