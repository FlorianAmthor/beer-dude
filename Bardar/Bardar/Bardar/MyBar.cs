﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bardar
{ 
    public class MyBar
    {
        //<Attributes>

        //unique id for the bar in MongoDB
        public string _id { get; set; }

        //name of the bar
        public string name { get; set; }

        //MyContact consists of a unique id for MongoDB, a telephone number, the corresponding email and the address of the location
        public MyContact contact { get; set; }

        //rating of the bar
        public int rating{ get; set; }

        //contains the prices of the beverages
        public List<MyBeverage> menu { get; set; }

        //opening hours of the bar
        public List<string> openingTimes { get; set; }

        //URI to the bar image
        public Uri imageUri { get; set; }

        //</Attributes>
        public MyBar()
        {
        }
    }
}
