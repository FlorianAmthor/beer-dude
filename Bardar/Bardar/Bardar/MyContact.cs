﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bardar
{
    public class MyContact
    {
        //<Attributes>

        //telephone number for the bar
        public string tel { get; set; }

        //email address of the bar
        public string email { get; set; }

        //city name of the location of the bar
        public string city { get; set; }

        //street name of the location of the bar
        public string street { get; set; }

        //house number of the location of the bar
        public string number { get; set; }

        //additional description like house number + letter : 9a or 9b
        //additional description like go up the stairs then the first door to the left or right
        public string addInfo { get; set; }

        //ZIP code of the location of the bar
        public string ZIP { get; set; }

        //latitude = coords[0], longitude = coords[1]
        public float[] coords{ get; set; }

        //</Attributes>
        public MyContact()
        {

        }
    }
}
