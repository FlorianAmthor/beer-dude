﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Storage.Streams;
using Windows.System;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace Bardar.Universal.Windows.App
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class SearchFunction : Page
    {
        //Attributes
        public static SearchFunction _searchPage;
        private List<MyBar> _currentBarList;
        private Geolocator _geolocator;
        private MapIcon _mapIcon1;
        private Geoposition _pos;
        private BasicGeoposition _currentpos;
        private IRandomAccessStreamReference _barPin;
        private IRandomAccessStreamReference _userPin;
        public SearchFunction()
        {
            InitializeComponent();
            _currentBarList = new List<MyBar>();
            _mapIcon1 = new MapIcon();
            LoadBars();
            _searchPage = this;
            _barPin = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/mapIcon_beer.png"));
            _userPin = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/mapIcon_user_pos.png"));
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (Frame.CanGoBack)
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            }
            else
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            }
        }

        private void Temp_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var item = sender as ListBoxItem;
            Frame.Navigate(typeof(DetailedView), item.Tag);
        }

        private async void LoadBars()
        {
            PivotItem actPivot = (PivotItem)rootPivot.SelectedItem;
            System.Net.HttpStatusCode response;
            if (APIConstant.userIsLoggedIn)
            {
                try
                {
                    List<string> barIdList = new List<string>();
                    foreach (var item in MainPage.currentUser.favourites)
                    {
                        barIdList.Add(item._id);
                    }
                    response = await MainPage.MyClient.UpdateFavourites(barIdList);
                    _currentBarList = await MainPage.MyClient.GetMyBarListRequest();
                    SearchListBox.Items.Clear();
                    foreach (var item in _currentBarList)
                    {   //List
                        ListBoxItem temp = new ListBoxItem { Content = item.name, Tag = item._id };
                        temp.Tapped += Temp_Tapped;
                        temp.Background = new SolidColorBrush(Colors.White);
                        temp.Foreground = new SolidColorBrush(Colors.Black);
                        SearchListBox.Items.Add(temp);
                    }
                }
                catch (Exception)
                {

                    MainPage.ShowGeneralMessage("Ein unbekannter Fehler ist aufgetreten!", MainPage.NotifyType.ErrorMessage);
                }
            }
            else
            {
                List<string> barIdList = new List<string>();
                foreach (var item in MainPage.currentUser.favourites)
                {
                    barIdList.Add(item._id);
                }
                _currentBarList = await MainPage.MyClient.GetMyBarListRequest();
                SearchListBox.Items.Clear();
                foreach (var item in _currentBarList)
                {   //List
                    ListBoxItem temp = new ListBoxItem { Content = item.name, Tag = item._id };
                    temp.Tapped += Temp_Tapped;
                    temp.Background = new SolidColorBrush(Colors.White);
                    temp.Foreground = new SolidColorBrush(Colors.Black);
                    SearchListBox.Items.Add(temp);
                }

            }



            //Location based services
            var accessStatus = await Geolocator.RequestAccessAsync(); //Ask for current position

            switch (accessStatus)
            {
                case GeolocationAccessStatus.Allowed:
                    HideLocationDisabledMessage();
                    //_rootPage.NotifyUser("Waiting for update...", NotifyType.StatusMessage);

                    // If DesiredAccuracy or DesiredAccuracyInMeters are not set (or value is 0), DesiredAccuracy.Default is used.
                    _geolocator = new Geolocator() { ReportInterval = 2000 };//Create geolocator and define periodic-based tracking (2 second interval)
                    _geolocator.DesiredAccuracy = PositionAccuracy.Default; //Enable energy saving
                    _geolocator.PositionChanged += OnPositionChanged;
                    _geolocator.StatusChanged += OnStatusChanged;

                    // Subscribe to the StatusChanged event to get updates of location status changes.
                    //_geolocator.StatusChanged += OnStatusChanged;

                    // Carry out the operation.
                    _pos = await _geolocator.GetGeopositionAsync();
                    _currentpos = new BasicGeoposition { Latitude = _pos.Coordinate.Point.Position.Latitude, Longitude = _pos.Coordinate.Point.Position.Longitude };
                    Geopoint currentPoint = new Geopoint(_currentpos);
                    MapControl1.Center = currentPoint;
                    UpdateLocationData(_pos);
                    MapControl1.ZoomLevel = 14;

                    foreach (var item in _currentBarList)
                    {
                        //AddBarPins(item.contact.city + "/" + item.contact.ZIP + "/" + item.contact.street + "/" + item.contact.number + "/" + item.name);
                        AddBarPins(item);
                    }

                    break;

                case GeolocationAccessStatus.Denied:
                    //_rootPage.NotifyUser("Access to location is denied.", NotifyType.ErrorMessage);
                    //MainPage.ShowGeneralMessage("Access to location denied", MainPage.NotifyType.ErrorMessage);
                    //LocationDisabledMessage.Visibility = Visibility.Visible;
                    //UpdateLocationData(null);
                    ShowLocationDisabledMessage();
                    break;

                case GeolocationAccessStatus.Unspecified:
                    //_rootPage.NotifyUser("Unspecified error.", NotifyType.ErrorMessage);
                    MainPage.ShowGeneralMessage("Unspecified error.", MainPage.NotifyType.ErrorMessage);
                    //UpdateLocationData(null);
                    break;
            }
        }

        private void AddBarPins(MyBar bar)
        {

            // List<String> barNames = geoAddress.Split('/').ToList();
            BasicGeoposition queryHint = new BasicGeoposition
            {
                Latitude = _pos.Coordinate.Point.Position.Latitude,
                Longitude = _pos.Coordinate.Point.Position.Longitude
            };
            var hintPoint = new Geopoint(queryHint);
            var snPosition = new BasicGeoposition() { Latitude = bar.contact.coords[0], Longitude = bar.contact.coords[1] };
            var snPoint = new Geopoint(snPosition);
            var barIcon = new MapIcon
            {
                Location = snPoint,
                NormalizedAnchorPoint = new Point(0.5, 1.0),
                Title = bar.name,
                Image = _barPin,
                ZIndex = 0
            };

            // Add the MapIcon to the map.
            MapControl1.MapElements.Add(barIcon);

        }

        private async void OnPositionChanged(Geolocator sender, PositionChangedEventArgs e)
        {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                //_rootPage.NotifyUser("Location updated.", NotifyType.StatusMessage);
                MapControl1.MapElements.Remove(_mapIcon1);
                UpdateLocationData(e.Position);
            });
        }

        private async void OnStatusChanged(Geolocator sender, StatusChangedEventArgs e)
        {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                // Show the location setting message only if status is disabled.
                //LocationDisabledMessage.Visibility = Visibility.Collapsed;

                switch (e.Status)
                {
                    case PositionStatus.Ready:
                        HideLocationDisabledMessage();
                        // Location platform is providing valid data.
                        //ScenarioOutput_Status.Text = "Ready";
                        //_rootPage.NotifyUser("Location platform is ready.", NotifyType.StatusMessage);
                        break;

                    case PositionStatus.Initializing:
                        HideLocationDisabledMessage();
                        // Location platform is attempting to acquire a fix. 
                        //ScenarioOutput_Status.Text = "Initializing";
                        //_rootPage.NotifyUser("Location platform is attempting to obtain a position.", NotifyType.StatusMessage);
                        break;

                    case PositionStatus.NoData:
                        HideLocationDisabledMessage();
                        // Location platform could not obtain location data.
                        //ScenarioOutput_Status.Text = "No data";
                        //_rootPage.NotifyUser("Not able to determine the location.", NotifyType.ErrorMessage);
                        break;

                    case PositionStatus.Disabled:
                        // The permission to access location data is denied by the user or other policies.
                        //ScenarioOutput_Status.Text = "Disabled";
                        //_rootPage.NotifyUser("Access to location is denied.", NotifyType.ErrorMessage);

                        // Show message to the user to go to location settings.
                        //LocationDisabledMessage.Visibility = Visibility.Visible;

                        // Clear any cached location data.
                        //searchPage.ShowGeneralMessage("Unspecified error.", MainPage.NotifyType.ErrorMessage);
                        ShowLocationDisabledMessage();
                        //UpdateLocationData(null);
                        break;

                    case PositionStatus.NotInitialized:
                        // The location platform is not initialized. This indicates that the application 
                        // has not made a request for location data.
                        //ScenarioOutput_Status.Text = "Not initialized";
                        //_rootPage.NotifyUser("No request for location is made yet.", NotifyType.StatusMessage);
                        break;

                    case PositionStatus.NotAvailable:
                        // The location platform is not available on this version of the OS.
                        //ScenarioOutput_Status.Text = "Not available";
                        //_rootPage.NotifyUser("Location is not available on this version of the OS.", NotifyType.ErrorMessage);
                        break;

                    default:
                        //ScenarioOutput_Status.Text = "Unknown";
                        //_rootPage.NotifyUser(string.Empty, NotifyType.StatusMessage);
                        break;
                }
            });
        }

        private void UpdateLocationData(Geoposition geo)
        {
            _currentpos = new BasicGeoposition { Latitude = geo.Coordinate.Point.Position.Latitude, Longitude = geo.Coordinate.Point.Position.Longitude };
            var currentPoint = new Geopoint(_currentpos);

            _mapIcon1.Location = currentPoint;
            _mapIcon1.NormalizedAnchorPoint = new Point(0.5, 1.0);
            _mapIcon1.Image = _userPin;
            _mapIcon1.ZIndex = 0;
            // Add the MapIcon to the map.
            MapControl1.MapElements.Add(_mapIcon1);
        }

        public async void ShowLocationDisabledMessage()
        {
            await LocationDiasbledDialog.ShowAsync();
        }

        public void HideLocationDisabledMessage()
        {
            LocationDiasbledDialog.Hide();
        }



        public void FillAfterSearch(List<MyBar> list)
        {
            if (SearchListBox.Items == null) return;
            SearchListBox.Items.Clear();
            foreach (var item in list)
            {
                var temp = new ListBoxItem { Content = item.name, Tag = item._id };
                temp.Tapped += Temp_Tapped;
                SearchListBox.Items.Add(temp);
            }
        }

        private void MapControl1_MapElementClick(MapControl sender, MapElementClickEventArgs args)
        {
            var myClickedIcon = args.MapElements.FirstOrDefault(x => x is MapIcon) as MapIcon;
            var bar = _currentBarList.Find(x => x.name.Equals(myClickedIcon?.Title));
            this.Frame.Navigate(typeof(DetailedView), bar._id);
        }

        private void RootPivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var actPivot = rootPivot.SelectedItem as PivotItem;
            switch (actPivot.Name)
            {
                case "PivotItemKarte":
                    ListPivotHeader.Foreground = new SolidColorBrush(Colors.Gray);
                    MapPivotHeader.Foreground = new SolidColorBrush(Colors.White);
                    break;
                case "PivotItemListe":
                    ListPivotHeader.Foreground = new SolidColorBrush(Colors.White);
                    MapPivotHeader.Foreground = new SolidColorBrush(Colors.Gray);
                    break;
                default:
                    break;
            }
        }

        private async void LocationDiasbledDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var result = await Launcher.LaunchUriAsync(new Uri("ms-settings:privacy-location"));
        }
    }
}
