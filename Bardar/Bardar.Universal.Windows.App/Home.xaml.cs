﻿using System.Collections.Generic;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Imaging;
// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace Bardar.Universal.Windows.App
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class Home : Page
    {
        public static Home homePage;

        public Home()
        {
            this.InitializeComponent();
            homePage = this;
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            if (CustomSplashScreen.splashPage.favList.Count > 0)
            {
                MainPage.currentUser.favourites = await MainPage.current.LoadUserFavourites();
            }
            InitializeShortCuts();
        }

        public async void InitializeShortCuts()
        {
            bool addSmth = false;
            List<MyBar> barList = new List<MyBar>();
            if (APIConstant.userIsLoggedIn)
            {
                if (MainPage.currentUser.favourites.Count > 0)
                {
                    NoFavouritesTextBlock.Visibility = Visibility.Collapsed;
                    List<string> barIdList = new List<string>();
                    foreach (var item in MainPage.currentUser.favourites)
                    {
                        barIdList.Add(item._id);
                    }
                    await MainPage.MyClient.UpdateFavourites(barIdList);
                    barList = await MainPage.MyClient.GetFavourites();
                    addSmth = true;
                }
                else
                {
                    NoFavouritesTextBlock.Visibility = Visibility.Visible;
                }
            }
            else
            {
                if (MainPage.currentUser.favourites.Count > 0)
                {
                    NoFavouritesTextBlock.Visibility = Visibility.Collapsed;
                    barList = MainPage.currentUser.favourites;
                    addSmth = true;
                }
                else
                {
                    NoFavouritesTextBlock.Visibility = Visibility.Visible;
                }
            }

            if (addSmth)
            {
                int rows = 0;
                int actColumn = 0;
                foreach (var item in barList)
                {
                    StackPanel sp = new StackPanel();
                    //initialize image
                    Image img = new Image();
                    img.MaxHeight = 100;
                    img.MaxWidth = 100;
                    img.Margin = new Thickness(5);
                    var bitmapImage = new BitmapImage();
                    bitmapImage.UriSource = item.imageUri;
                    img.Source = bitmapImage;
                    //initialize barname in textblock
                    TextBlock barName = new TextBlock();
                    barName.TextWrapping = TextWrapping.WrapWholeWords;
                    barName.Text = item.name;

                    sp.Children.Add(img);
                    sp.Children.Add(barName);
                    sp.Tapped += Sp_Tapped;
                    sp.Tag = item._id;

                    HomeGrid.Children.Add(sp);
                    Grid.SetRow(sp, rows);
                    Grid.SetColumn(sp, actColumn);
                    actColumn++;
                    if (HomeGrid.Children.Count % 3 == 0)
                    {
                        RowDefinition rowDef = new RowDefinition();
                        rowDef.Height = GridLength.Auto;
                        HomeGrid.RowDefinitions.Add(new RowDefinition());
                        rows++;
                        actColumn = 0;
                    }
                }
            }
        }

        private void Sp_Tapped(object sender, global::Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            var sp = sender as StackPanel;
            this.Frame.Navigate(typeof(DetailedView), sp.Tag.ToString());
        }
    }
}
