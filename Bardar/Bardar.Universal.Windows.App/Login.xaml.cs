﻿using System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace Bardar.Universal.Windows.App
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class Login : Page
    {
        private MenuFlyoutItem _item;
        public Login()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            }
            else
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            }

            _item = e.Parameter as MenuFlyoutItem;
        }

        private void RegisterButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Register));
        }

        private async void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UserEmailBox.Text == "" || UserPasswordBox.Password == "")
                {
                    MainPage.ShowGeneralMessage("Email oder Passwort darf nicht leer sein!", MainPage.NotifyType.ErrorMessage);
                }
                else
                {
                    var statusCode = await MainPage.MyClient.FetchLoginToken(UserEmailBox.Text, UserPasswordBox.Password);

                    if (statusCode == System.Net.HttpStatusCode.OK)
                    {
                        MainPage.MyClient.DefaultRequestHeaders.Authorization = APIConstant.MyClientAuthenticationHeader;
                        MainPage.currentUser = await MainPage.MyClient.GetUserInfo();
                        APIConstant.userIsLoggedIn = true;
                        MainPage.current.UpdateUserItem();
                        //currentUserImage.Source = currentUser.image;
                        if (_item.Name == "UserItemLoginFlyout")
                        {
                            this.Frame.Navigate(typeof(Home));
                        }
                        else
                        {
                            this.Frame.Navigate(typeof(Settings));
                        }
                    }
                    else
                    {
                        APIConstant.userIsLoggedIn = false;
                        MainPage.ShowGeneralMessage(MainPage.MyClient.HandleHttpCode(statusCode), MainPage.NotifyType.ErrorMessage);
                    }
                }
            }
            catch (Exception Error)
            {
                MainPage.SendBugReport(Error, this);
            }
        }

        private async void ChangePasswordLink_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                    var result = await EmailInputDialog.ShowAsync();

                    if (result == ContentDialogResult.Primary && EmailContenDialogTextBox.Text != "")
                    {
                        this.Frame.Navigate(typeof(PasswordChange), EmailContenDialogTextBox.Text);
                    }
                    else if (result == ContentDialogResult.Primary && EmailContenDialogTextBox.Text == "")
                    {
                        MainPage.ShowGeneralMessage("Email darf nicht leer sein!", MainPage.NotifyType.ErrorMessage);
                    }
            }
            catch (Exception Error)
            {
                MainPage.SendBugReport(Error, this);
            }
        }

        private async void EmailInputDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            try
            {
                if (EmailContenDialogTextBox.Text != "")
                {
                    var statusCode = await MainPage.MyClient.SendPasswordCodeToEmail(EmailContenDialogTextBox.Text);
                }
            }
            catch (Exception Error)
            {
                MainPage.SendBugReport(Error, this);
            }
        }

        /*private async void Facebook_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                string FacebookURL = "https://www.facebook.com/dialog/oauth?client_id=" + "240907576280293" + "&redirect_uri=" + "https://www.facebook.com/connect/login_success.html" + "&scope=public_profile&display=popup&response_type=token";

                Uri StartUri = new Uri(FacebookURL);
                Uri EndUri = new Uri("https://www.facebook.com/connect/login_success.html");

                //rootPage.NotifyUser("Navigating to: " + FacebookURL, NotifyType.StatusMessage);

                WebAuthenticationResult WebAuthenticationResult = await WebAuthenticationBroker.AuthenticateAsync(WebAuthenticationOptions.None, StartUri, EndUri);
                if (WebAuthenticationResult.ResponseStatus == WebAuthenticationStatus.Success)
                {
                    //OutputToken(WebAuthenticationResult.ResponseData.ToString());
                    //await GetFacebookUserNameAsync(WebAuthenticationResult.ResponseData.ToString());
                    string[] token = WebAuthenticationResult.ResponseData.ToString().Split('=');
                    var statusCode = await MainPage.MyClient.facebookLogin(token[1]);
                    if (statusCode == System.Net.HttpStatusCode.OK)
                    {
                        MainPage.currentUser.email = EmailContenDialogTextBox.Text;
                    }
                    else
                    {
                        MainPage.ShowGeneralMessage("Email Adresse nicht gefunden", MainPage.NotifyType.ErrorMessage);
                        MainPage.currentUser.email = "";
                    }
                }
                else if (WebAuthenticationResult.ResponseStatus == WebAuthenticationStatus.ErrorHttp)
                {
                    //OutputToken("HTTP Error returned by AuthenticateAsync() : " + WebAuthenticationResult.ResponseErrorDetail.ToString());
                }
                else
                {
                    //OutputToken("Error returned by AuthenticateAsync() : " + WebAuthenticationResult.ResponseStatus.ToString());
                }

            }
            catch (Exception Error)
            {
                MainPage.SendBugReport(Error, this);
            }
        }
        //Grab the username for example

        private async Task GetFacebookUserNameAsync(string webAuthResultResponseData)
        {
            try
            {
                //Get Access Token first
                string responseData = webAuthResultResponseData.Substring(webAuthResultResponseData.IndexOf("access_token"));
                String[] keyValPairs = responseData.Split('&');
                string access_token = null;
                string expires_in = null;
                for (int i = 0; i < keyValPairs.Length; i++)
                {
                    String[] splits = keyValPairs[i].Split('=');
                    switch (splits[0])
                    {
                        case "access_token":
                            access_token = splits[1]; //you may want to store access_token for further use. Look at Scenario 5 (Account Management).
                            break;
                        case "expires_in":
                            expires_in = splits[1];
                            break;
                    }
                }

                //rootPage.NotifyUser("access_token = " + access_token, NotifyType.StatusMessage);
                //Request User info.
                HttpClient httpClient = new HttpClient();
                string response = await httpClient.GetStringAsync(new Uri("https://graph.facebook.com/me?access_token=" + access_token));
                JsonObject value = JsonValue.Parse(response).GetObject();
                string facebookUserName = value.GetNamedString("name");

                //rootPage.NotifyUser(facebookUserName + " is connected!", NotifyType.StatusMessage);
            }
            catch (Exception Error)
            {
                MainPage.SendBugReport(Error, this);
            }
        }*/
    }
}
