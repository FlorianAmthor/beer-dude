﻿using System;
using System.Collections.Generic;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Bardar.Universal.Windows.App
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class EditFavourites : Page
    {
        public static EditFavourites favPage;
        public EditFavourites()
        {
            this.InitializeComponent();
            favPage = this;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            }
            else
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            }

            try
            {

                foreach (var item in MainPage.currentUser.favourites)
                {
                    ListViewItem fav = new ListViewItem();
                    StackPanel stackp = new StackPanel();
                    CheckBox check = new CheckBox();
                    TextBlock favName = new TextBlock();
                    stackp.Orientation = Orientation.Horizontal;
                    favName.Text = item.name;
                    favName.VerticalAlignment = VerticalAlignment.Center;

                    check.IsChecked = false;

                    stackp.Children.Add(check);
                    stackp.Children.Add(favName);

                    fav.Content = stackp;
                    FavListView.Items.Add(fav);
                }
            }
            catch (Exception Error)
            {
                MainPage.SendBugReport(Error, this);
            }
        }

        public void CheckAllFavourites()
        {
            try
            {
                foreach (ListViewItem item in FavListView.Items)
                {
                    var panel = item.Content as StackPanel;
                    var checkBox = panel.Children[0] as CheckBox;
                    checkBox.IsChecked = !checkBox.IsChecked;
                }
            }
            catch (Exception Error)
            {
                MainPage.SendBugReport(Error, this);
            }
        }

        public async void DeleteSelectedFavourites()
        {
            try
            {
                List<ListViewItem> delValues = new List<ListViewItem>();
                for (int i = 0; i < FavListView.Items.Count; i++)
                {
                    MyBar bar = new MyBar();
                    var item = FavListView.Items[i] as ListViewItem;
                    var panel = item.Content as StackPanel;
                    var checkBox = panel.Children[0] as CheckBox;
                    var favName = panel.Children[1] as TextBlock;
                    bar.name = favName.Text;
                    if (checkBox.IsChecked == true)
                    {
                        delValues.Add(item);
                        MainPage.currentUser.favourites.Remove(bar);
                    }
                }
                foreach (ListViewItem favitem in delValues)
                {
                    FavListView.Items.Remove(FavListView.Items.Remove(favitem));
                }
                if (APIConstant.userIsLoggedIn)
                {
                    List<string> barIdList = new List<string>();
                    foreach (var item in MainPage.currentUser.favourites)
                    {
                        barIdList.Add(item._id);
                    }
                    var response = await MainPage.MyClient.UpdateFavourites(barIdList);
                }
                MainPage.ShowGeneralMessage("Ausgewählte Favoriten entfernt!", MainPage.NotifyType.StatusMessage);
            }
            catch (Exception Error)
            {
                MainPage.SendBugReport(Error, this);
            }
        }
    }
}
