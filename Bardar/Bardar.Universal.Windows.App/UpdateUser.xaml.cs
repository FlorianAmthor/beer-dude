﻿using System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace Bardar.Universal.Windows.App
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class UpdateUser : Page
    {
        public UpdateUser()
        {
            this.InitializeComponent();
            ActualEmailTextBlock.Text = MainPage.currentUser.email;
            if (MainPage.currentUser.name != null)
            {
                ActualProfileNameTextBlock.Text = MainPage.currentUser.name;
            }
            else
            {
                ActualProfileNameTextBlock.Text = "Noch kein Name festgelegt";
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            }
            else
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            }
        }

        private async void SendChangesButton_Click(object sender, RoutedEventArgs e)
        {
            if (NewEmailBox.Text != "" || NewProfileNameBox.Text != "")
            {
                try
                {
                    var statusCode = await MainPage.MyClient.UpdateUserInfo(MainPage.currentUser.email, NewProfileNameBox.Text, NewEmailBox.Text);
                    if (statusCode == System.Net.HttpStatusCode.OK)
                    {
                        if (NewEmailBox.Text != "")
                        {
                            MainPage.currentUser = new MyUser();
                            MainPage.currentUser.name = "Gast";
                            MainPage.currentUser.email = "gast@beerdude.de";
                            APIConstant.MyClientAuthenticationHeader = null;
                            MainPage.MyClient.DefaultRequestHeaders.Authorization = null;
                            APIConstant.userIsLoggedIn = false;
                            MainPage.ShowGeneralMessage("Email Adresse erfolgreich geändert!\r\nErneuter Login erforderlich!", MainPage.NotifyType.StatusMessage);
                        }
                        else if (NewProfileNameBox.Text != "")
                        {
                            MainPage.currentUser.name = NewProfileNameBox.Text;
                            MainPage.current.UpdateUserItem();
                            MainPage.ShowGeneralMessage("Profilname erfolgreich geändert!", MainPage.NotifyType.StatusMessage);
                        }
                        this.Frame.Navigate(typeof(Settings));
                    }
                }
                catch (Exception Error)
                {
                    MainPage.ShowGeneralMessage("Ein unbekannter Fehler ist aufgetreten!", MainPage.NotifyType.StatusMessage);
                    MainPage.SendBugReport(Error, this);
                }
            }
        }
    }
}
