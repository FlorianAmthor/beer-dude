﻿using System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace Bardar.Universal.Windows.App
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class Settings : Page
    {
        public Settings()
        {
            this.InitializeComponent();
            if (APIConstant.userIsLoggedIn)
            {
                if (MainPage.currentUser.name == null)
                {
                    UserNameTextBlock.Name = "Kein Profilname gewählt";
                    ChangeProfileNameLink.Visibility = Visibility.Visible;
                }
                else
                {
                    UserNameTextBlock.Text = MainPage.currentUser.name;
                    ChangeProfileNameLink.Visibility = Visibility.Collapsed;
                }
                UserEmailTextBlock.Text = MainPage.currentUser.email;
            }
            else
            {
                UserNameTextBlock.Text = MainPage.currentUser.name;
                UserEmailTextBlock.Text = MainPage.currentUser.email;
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            }
            else
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            }
        }

        private  void SettingsItem_Tapped(object sender, TappedRoutedEventArgs e)
        {
            try
            {
                var listItem = sender as ListBoxItem;

                switch (listItem.Name)
                {
                    case "AccountItem":
                        if (!APIConstant.userIsLoggedIn)
                        {
                            MainPage.ShowGeneralMessage("Du musst eingeloggt sein um das zu bearbeiten!", MainPage.NotifyType.ErrorMessage);
                        }
                        else
                        {
                            this.Frame.Navigate(typeof(AccountSettings));
                        }
                        break;
                    case "AppInfoItem":
                        this.Frame.Navigate(typeof(AppInfo));
                        break;
                    case "UpdateFavoritesItem":
                        this.Frame.Navigate(typeof(EditFavourites));
                        break;
                    case "FeedbackItem":
                        this.Frame.Navigate(typeof(FeedBackPage));
                        break;
                    default:
                        break;
                }
            }
            catch (Exception Error)
            {
                MainPage.SendBugReport(Error, this);
            }
        }

        private void ChangeProfileNameLink_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(UpdateUser));
        }
    }
}
