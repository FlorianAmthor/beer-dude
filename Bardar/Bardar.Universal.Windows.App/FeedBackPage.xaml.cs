﻿using System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Bardar.Universal.Windows.App
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class FeedBackPage : Page
    {
        public FeedBackPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            }
            else
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            }
        }

        private async void SendEmailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Net.HttpStatusCode statusCode;
                if (FeedBackRadioButton.IsChecked == true)
                {
                    statusCode = await MainPage.MyClient.SendReport("feedback", MainPage.currentUser.email, SubjectTextBox.Text, MessageTextBox.Text);
                }
                else
                {
                    statusCode = await MainPage.MyClient.SendReport("bug", MainPage.currentUser.email, SubjectTextBox.Text, MessageTextBox.Text);
                }
                if (statusCode == System.Net.HttpStatusCode.OK)
                {
                    SubjectTextBox.Text = "";
                    MessageTextBox.Text = "";
                    MainPage.ShowGeneralMessage($"Feedback wurde erfolgreich gesendet!", MainPage.NotifyType.StatusMessage);
                }
                else
                {
                    MainPage.ShowGeneralMessage(MainPage.MyClient.HandleHttpCode(statusCode), MainPage.NotifyType.ErrorMessage);
                }
            }
            catch (Exception Error)
            {
                MainPage.SendBugReport(Error, this);
            }
        }
    }
}