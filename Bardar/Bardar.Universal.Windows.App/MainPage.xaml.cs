﻿using System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using System.Collections.Generic;
using Windows.UI.Xaml.Input;
using System.Threading.Tasks;

// Die Vorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409 dokumentiert.

namespace Bardar.Universal.Windows.App
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public enum NotifyType
        {
            StatusMessage,
            ErrorMessage,
            LocationDisabledMessage
        };

        public static MainPage current;
        public static MyHttpClient MyClient;
        public static MyUser currentUser;

        public MainPage()
        {
            this.InitializeComponent();
            StartFrame.Navigated += StartFrame_Navigated;
            SystemNavigationManager.GetForCurrentView().BackRequested += MainPage_BackRequested;

            MyClient = new MyHttpClient();
            if (APIConstant.userDataHasBeenLoad)
            {
               MyClient.DefaultRequestHeaders.Authorization = APIConstant.MyClientAuthenticationHeader;
            }
            else
            {
                currentUser = new MyUser();
                currentUser.name = "Gast";
                currentUser.email = "gast@beerdude.de";
            }
            UpdateUserItem();
            current = this;

            StartFrame.Navigate(typeof(Home));
        }

        private void StartFrame_Navigated(object sender, NavigationEventArgs e)
        {
            MainRelativePanel.Visibility = Visibility.Visible;
            MoreButton.Visibility = Visibility.Collapsed;
            SearchButton.Visibility = Visibility.Collapsed;
            DeleteFavButton.Visibility = Visibility.Collapsed;
            FavCheckBox.Visibility = Visibility.Collapsed;
            MainSplitView.IsPaneOpen = false;

            if (StartFrame.CurrentSourcePageType.Name == "Home")
            {
                PageTitleBox.Visibility = Visibility.Visible;
                PageTitleBox.Text = "Start";
                HomeItem.IsSelected = true;
                StartFrame.BackStack.Clear();
            }
            else if (StartFrame.CurrentSourcePageType.Name == "SearchFunction")
            {
                PageTitleBox.Visibility = Visibility.Visible;
                PageTitleBox.Text = "Barsuche";
                SearchButton.Visibility = Visibility.Visible;
            }
            else if (StartFrame.CurrentSourcePageType.Name == "NewsFeed")
            {
                PageTitleBox.Visibility = Visibility.Visible;
                PageTitleBox.Text = "Newsfeed";
                SearchButton.Visibility = Visibility.Visible;
            }
            else if (StartFrame.CurrentSourcePageType.Name == "Settings")
            {
                PageTitleBox.Visibility = Visibility.Visible;
                PageTitleBox.Text = "Einstellungen";
                MoreButton.Visibility = Visibility.Visible;
            }
            else if (StartFrame.CurrentSourcePageType.Name == "DetailedView")
            {
                PageTitleBox.Visibility = Visibility.Visible;
                PageTitleBox.Text = "Bar Infos";
            }
            else if (StartFrame.CurrentSourcePageType.Name == "DetailedViewEvent")
            {
                PageTitleBox.Visibility = Visibility.Visible;
                PageTitleBox.Text = "Event Infos";
            }
            else if (StartFrame.CurrentSourcePageType.Name == "Login")
            {
                PageTitleBox.Visibility = Visibility.Visible;
                PageTitleBox.Text = "Login";
            }
            else if (StartFrame.CurrentSourcePageType.Name == "Register")
            {
                PageTitleBox.Visibility = Visibility.Visible;
                PageTitleBox.Text = "Registrieren";
            }
            else if (StartFrame.CurrentSourcePageType.Name == "AccountSettings")
            {
                PageTitleBox.Visibility = Visibility.Visible;
                PageTitleBox.Text = "Account";
            }
            else if (StartFrame.CurrentSourcePageType.Name == "AppInfo")
            {
                PageTitleBox.Visibility = Visibility.Visible;
                PageTitleBox.Text = "Über und Hilfe";
                MainRelativePanel.Visibility = Visibility.Collapsed;
            }
            else if (StartFrame.CurrentSourcePageType.Name == "PasswordChange")
            {
                PageTitleBox.Visibility = Visibility.Visible;
                PageTitleBox.Text = "Passwortänderung";
            }
            else if (StartFrame.CurrentSourcePageType.Name == "EditFavourites")
            {
                PageTitleBox.Visibility = Visibility.Collapsed;
                DeleteFavButton.Visibility = Visibility.Visible;
                FavCheckBox.IsChecked = false;
                FavCheckBox.Visibility = Visibility.Visible;
            }
            SearchBox.Visibility = Visibility.Collapsed;
        }

        private void MainPage_BackRequested(object sender, BackRequestedEventArgs e)
        {
            if (StartFrame.CanGoBack)
            {
                if (StartFrame.CurrentSourcePageType.Name == "SearchFunction" ||
                    StartFrame.CurrentSourcePageType.Name == "NewsFeed" ||
                    StartFrame.CurrentSourcePageType.Name == "Settings")
                {
                    e.Handled = true;
                    StartFrame.GoBack();
                    StartFrame.Navigate(typeof(Home));
                    StartFrame.BackStack.Clear();
                    HomeItem.IsSelected = true;
                }
                else
                {
                    e.Handled = true;
                    StartFrame.GoBack();
                }
            }
        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            MainSplitView.IsPaneOpen = !MainSplitView.IsPaneOpen;
        }

        private void HamburgerListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MainSplitView.IsPaneOpen = false;
            SettingsItem.IsSelected = false;
            if (HomeItem.IsSelected && !StartFrame.CurrentSourcePageType.Equals(typeof(Home)))
            { 
                StartFrame.Navigate(typeof(Home));
            }
            else if (SearchItem.IsSelected && !StartFrame.CurrentSourcePageType.Equals(typeof(SearchFunction)))
            {
                StartFrame.Navigate(typeof(SearchFunction));
            }
            else if (NewsFeedItem.IsSelected && !StartFrame.CurrentSourcePageType.Equals(typeof(NewsFeed)))
            {
                StartFrame.Navigate(typeof(NewsFeed));
            }
        }

        private async void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            if (SearchBox.Visibility == Visibility.Visible)
            {
                switch (StartFrame.CurrentSourcePageType.Name)
                {
                    case "SearchFunction":
                        List<MyBar> MyBarList = await MyClient.BarSearchRequest(SearchBox.Text);
                        SearchFunction._searchPage.FillAfterSearch(MyBarList);
                        break;
                    case "NewsFeed":
                        List<MyEvent> MyEventList = await MyClient.EventSearchRequest(SearchBox.Text);
                        NewsFeed.newsFeedPage.FillAfterSearch(MyEventList);
                        break;
                    default:
                        break;
                }
                SearchBox.Visibility = Visibility.Collapsed;
                PageTitleBox.Visibility = Visibility.Visible;
            }
            else
            {
                SearchBox.Visibility = Visibility.Visible;
                PageTitleBox.Visibility = Visibility.Collapsed;
                SearchBox.Focus(FocusState.Programmatic);
            }
        }

        private void SearchBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (SearchBox.Text == "")
            {
                SearchBox.Visibility = Visibility.Collapsed;
                PageTitleBox.Visibility = Visibility.Visible;
            }
        }

        public async static void ShowGeneralMessage(string msg, NotifyType type)
        {
            ContentDialog dialog = new ContentDialog();

            switch (type)
            {
                case NotifyType.StatusMessage:
                    
                    dialog.Title = "Information";
                    dialog.IsPrimaryButtonEnabled = true;
                    dialog.Content = msg;
                    dialog.PrimaryButtonText = "Ok";
                    break;
                case NotifyType.ErrorMessage:
                    dialog.Title = "Fehler";
                    dialog.IsPrimaryButtonEnabled = true;
                    dialog.Content = msg;
                    dialog.PrimaryButtonText = "Ok";
                    break;
                case NotifyType.LocationDisabledMessage:
                    dialog.Title = "Location disabled";
                    dialog.IsPrimaryButtonEnabled = true;
                    dialog.Content = msg;
                    dialog.PrimaryButtonText = "Settings";
                    break;
                default:
                    break;
            }
            var result = await dialog.ShowAsync();
        }

        private void MenuFlyoutItem_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var item = sender as MenuFlyoutItem;

            switch (item.Text)
            {
                case "Login":
                    StartFrame.Navigate(typeof(Login), item);
                    break;
                case "Logout":
                    currentUser = new MyUser();
                    currentUser.name = "Gast";
                    currentUser.email = "gast@beerdude.de";
                    APIConstant.MyClientAuthenticationHeader = null;
                    MyClient.DefaultRequestHeaders.Authorization = null;
                    APIConstant.userIsLoggedIn = false;
                    UpdateUserItem();
                    CustomSplashScreen.splashPage.favList.Clear();
                    //currentUserImage.Source = currentUser.image;
                    App.appData.localSettings.Values.Clear();
                    if (item.Name == "UserItemLogoutFlyout")
                    {
                        StartFrame.Navigate(typeof(Home));
                    }
                    else
                    {
                        StartFrame.Navigate(typeof(Settings));
                    }
                    ShowGeneralMessage("Erfolgreich ausgeloggt!", NotifyType.StatusMessage);
                    break;
                default:
                    break;
            }
        }

        private void MoreButton_Click(object sender, RoutedEventArgs e)
        {
            var flyout = sender as Button;
            if (APIConstant.userIsLoggedIn)
            {
                if (flyout.Name == "UserOptionsButton")
                {
                    UserItemLoginFlyout.Visibility = Visibility.Collapsed;
                    UserItemLogoutFlyout.Visibility = Visibility.Visible;
                }
                else
                {
                    LoginFlyout.Visibility = Visibility.Collapsed;
                    LogoutFlyout.Visibility = Visibility.Visible;
                }
            }
            else
            {
                if (flyout.Name == "UserOptionsButton")
                {
                    UserItemLoginFlyout.Visibility = Visibility.Visible;
                    UserItemLogoutFlyout.Visibility = Visibility.Collapsed;
                }
                else
                {
                    LoginFlyout.Visibility = Visibility.Visible;
                    LogoutFlyout.Visibility = Visibility.Collapsed;
                }
            }
        }

        private void DeleteFavButton_Click(object sender, RoutedEventArgs e)
        {
            EditFavourites.favPage.DeleteSelectedFavourites();
            FavCheckBox.IsChecked = false;
        }

        private void FavCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            EditFavourites.favPage.CheckAllFavourites();
        }

        public Task<List<MyBar>> LoadUserFavourites()
        {
            return MyClient.GetMyBarListByIdRequest(CustomSplashScreen.splashPage.favList);
        }

        public void UpdateUserItem()
        {
            if (currentUser.name == null)
            {
                currentUser.name = "Noch nicht gesetzt!";
            }
            currentUserName.Text = currentUser.name;
            currentUserEmail.Text = currentUser.email;
            //currentUserImage.Source = currentUser.image
        }

        private void OptionsListBox_Tapped(object sender, TappedRoutedEventArgs e)
        {
            HomeItem.IsSelected = false;
            NewsFeedItem.IsSelected = false;
            SearchItem.IsSelected = false;
            MainSplitView.IsPaneOpen = false;
            SettingsItem.IsSelected = true;
            if (!StartFrame.CurrentSourcePageType.Equals(typeof(Settings)))
            {
                StartFrame.Navigate(typeof(Settings));
            }
        }

        private void ItemListBox_Tapped(object sender, TappedRoutedEventArgs e)
        {
            MainSplitView.IsPaneOpen = false;
        }

        public async static void SendBugReport(Exception err, Page senderPage)
        {
            ContentDialog cd = new ContentDialog() { Title = "Unbekannter Fehler" };

            var textPanel = new StackPanel();

            //Add text to Panel
            textPanel.Children.Add(new TextBlock { Text = "Soll der Fehlerbericht gesendent werden?", TextWrapping = TextWrapping.Wrap });

            //Add textPanel to dialog
            cd.Content = textPanel;

            //Add Buttons
            cd.PrimaryButtonText = "Ja";
            cd.IsPrimaryButtonEnabled = true;

            cd.SecondaryButtonText = "Nein";
            cd.IsSecondaryButtonEnabled = true;

            var result = await cd.ShowAsync();

            if (result == ContentDialogResult.Primary)
            {
                string msg = $"Error Message: \r\n\t{err.Message} \r\n\r\n StackTrace: \r\n\t {err.StackTrace}";
                var statusCode = await MyClient.SendReport("bug", currentUser.email, $"Exception: {err.GetType().ToString()}", msg);
                if (statusCode == System.Net.HttpStatusCode.OK)
                {
                    ShowGeneralMessage("Fehler gesendet!", NotifyType.StatusMessage);
                }
                else
                {
                    ShowGeneralMessage(MyClient.HandleHttpCode(statusCode), NotifyType.StatusMessage);
                }
            }
        }
    }
}