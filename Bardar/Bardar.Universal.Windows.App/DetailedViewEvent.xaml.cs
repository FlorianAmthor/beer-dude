﻿using System;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

namespace Bardar.Universal.Windows.App
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class DetailedViewEvent : Page
    {
        private MyEvent currentEvent;
        public DetailedViewEvent()
        {
            this.InitializeComponent();
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            }
            else
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            }

            try
            {
                currentEvent = new MyEvent();
                var eventId = (string)e.Parameter;

                currentEvent = await MainPage.MyClient.GetMyEventRequest(eventId);

                EventNameTextBlock.Text = "Was: " + currentEvent.name;
                BarNameTextBlock.Text = "Wo: " + currentEvent.bar;
                EventDateTextBlock.Text = "Wann: " + currentEvent.date.Day.ToString() + "." + currentEvent.date.Month.ToString() + "." + currentEvent.date.Year.ToString();
                EventTimeTextBlock.Text = "Um: " + currentEvent.date.Hour.ToString() + ":" + currentEvent.date.Minute.ToString();
                EventDescriptionTextBlock.Text = currentEvent.description;
            }
            catch (Exception Error)
            {
                MainPage.SendBugReport(Error, this);
            }
        }

        private void BarNameTextBlock_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(DetailedView), currentEvent.barId);
        }
    }
}
