﻿using System;
using System.Collections.Generic;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace Bardar.Universal.Windows.App
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class NewsFeed : Page
    {
        public static NewsFeed newsFeedPage;
        private List<MyEvent> currentEventList;
        public NewsFeed()
        {
            this.InitializeComponent();
            LoadEvents();
            newsFeedPage = this;          
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            }
            else
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            }
        }

        private async void LoadEvents()
        {
            try
            {
                List<string> barIdList = new List<string>();
                foreach (var item in MainPage.currentUser.favourites)
                {
                    barIdList.Add(item._id);
                }

                System.Net.HttpStatusCode response;
                if(APIConstant.userIsLoggedIn)
                {
                    response = await MainPage.MyClient.UpdateFavourites(barIdList);
                }
                else
                {
                    response = System.Net.HttpStatusCode.OK;
                }

                if (response == System.Net.HttpStatusCode.OK)
                {
                    if (MainPage.currentUser.favourites.Count != 0)
                    {
                        barIdList.Clear();
                        foreach (var item in MainPage.currentUser.favourites)
                        {
                            barIdList.Add(item._id);
                        }
                        currentEventList = await MainPage.MyClient.GetFavouriteEvents(barIdList);
                        NewsFeedListBox.Items?.Clear();
                        foreach (var item in currentEventList)
                        {
                            ListBoxItem temp = new ListBoxItem();
                            temp.Content = item.name;
                            temp.Tag = item._id;
                            temp.Tapped += Temp_Tapped;
                            NewsFeedListBox.Items?.Add(temp);
                        }
                    }
                    else
                    {
                        currentEventList = await MainPage.MyClient.GetMyEventListRequest();
                        NewsFeedListBox.Items?.Clear();
                        foreach (var item in currentEventList)
                        {
                            ListBoxItem temp = new ListBoxItem();
                            temp.Content = item.name;
                            temp.Tag = item._id;
                            temp.Tapped += Temp_Tapped;
                            NewsFeedListBox.Items?.Add(temp);
                        }
                    }
                }
                else
                {
                    MainPage.ShowGeneralMessage(MainPage.MyClient.HandleHttpCode(response), MainPage.NotifyType.ErrorMessage);
                }
            }
            catch (Exception error)
            {
                MainPage.SendBugReport(error, this);
            }
        }

        private void Temp_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var item = sender as ListBoxItem;
            this.Frame.Navigate(typeof(DetailedViewEvent), item.Tag);
        }

        public void FillAfterSearch(List<MyEvent> list)
        {
            try
            {
                NewsFeedListBox.Items.Clear();
                foreach (var item in list)
                {
                    ListBoxItem temp = new ListBoxItem();
                    temp.Content = item.name;
                    temp.Tag = item._id;
                    temp.Tapped += Temp_Tapped;
                    NewsFeedListBox.Items.Add(temp);
                }
            }
            catch (Exception error)
            {
                MainPage.SendBugReport(error, this);
            }
        }
    }
}