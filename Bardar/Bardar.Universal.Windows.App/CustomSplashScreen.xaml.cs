﻿using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Bardar.Universal.Windows.App
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class CustomSplashScreen : Page
    {
        public static CustomSplashScreen splashPage;
        public List<string> favList;

        public CustomSplashScreen()
        {
            this.InitializeComponent();
            splashPage = this;
            favList = new List<string>();
            ExtendSplashScreen();
        }

        public void LoadUserInfoFromSettings()
        {
            try
            {
                if (App.appData.localSettings == null)
                {
                    APIConstant.userDataHasBeenLoad = false;
                }
                else
                {
                    MainPage.currentUser = new MyUser();
                    MainPage.currentUser.name = App.appData.localSettings.Values["currentUserName"].ToString();
                    MainPage.currentUser.email = App.appData.localSettings.Values["currentUserEmail"].ToString();
                    int favCount = (int)App.appData.localSettings.Values["userFavCount"];
                    favList = new List<string>();

                    for (int i = 0; i < favCount; i++)
                    {
                        favList.Add(App.appData.localSettings.Values[$"userFav{i}"].ToString());
                    }

                    APIConstant.userIsLoggedIn = (bool)App.appData.localSettings.Values["isUserLoggedIn"];
                    try
                    {
                        APIConstant.MyClientAuthenticationHeader = AuthenticationHeaderValue.Parse(App.appData.localSettings.Values["authHeader"].ToString());
                    }
                    catch (Exception e)
                    {
                        e.ToString();
                    }

                    APIConstant.userDataHasBeenLoad = true;
                }
            }
            catch (Exception)
            {
            }
        }

        private async void ExtendSplashScreen()
        {
            ProgressRingSplash.IsActive = true;
            App.appData.localSettings = App.appData.localSettings = ApplicationData.Current.LocalSettings;
            if (App.appData.localSettings.Values["currentUserName"] != null)
            {
               LoadUserInfoFromSettings();
            }
            await Task.Delay(TimeSpan.FromSeconds(3)); // set your desired delay  
            ProgressRingSplash.IsActive = false;
            Frame.Navigate(typeof(MainPage)); // call MainPage    
        }
    }
}
