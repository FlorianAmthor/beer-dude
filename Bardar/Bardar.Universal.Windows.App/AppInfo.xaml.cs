﻿using System;
using System.Linq;
using System.Reflection;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Email;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace Bardar.Universal.Windows.App
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class AppInfo : Page
    {
        public AppInfo()
        {
            this.InitializeComponent();
            Package package = Package.Current;
            AppNameTextBlock.Text = "Beer Dude";
            PackageId packageId = package.Id;
            PackageVersion version = packageId.Version;

            var currentAssembly = typeof(App).GetTypeInfo().Assembly;
            var customAttributes = currentAssembly.CustomAttributes;
            var list = customAttributes.ToList();
            var copyrightText = list[8].ConstructorArguments[0].Value.ToString();

            CopyRightTextBlock.Text = copyrightText;
            AppVersionTextBlock.Text = string.Format($"Version {version.Major}.{version.Minor}.{version.Build}");

        }

        private async void KontaktEmailHyperLink_Click(object sender, RoutedEventArgs e)
        {
            EmailMessage emailMessage = new EmailMessage();
            emailMessage.To.Add(new EmailRecipient("helpdesk.beerdude@gmail.com"));
            await EmailManager.ShowComposeNewEmailAsync(emailMessage);
        }

        private async void PrivacyPolicyHyperLink_Click(object sender, RoutedEventArgs e)
        {
           Uri policyUri = new Uri("https://beerdude.de/Datenschutzerklärung.html");
           await Launcher.LaunchUriAsync(policyUri);
        }
    }
}
