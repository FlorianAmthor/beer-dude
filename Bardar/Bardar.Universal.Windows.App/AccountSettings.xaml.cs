﻿using System;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace Bardar.Universal.Windows.App
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class AccountSettings : Page
    {
        public AccountSettings()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            }
            else
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            }
        }

        private async void AccountItem_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var item = sender as ListBoxItem;

            switch (item.Name)
            {
                case "DeleteAccountItem":
                    try
                    {
                        var result = await DeleteAccountDialog.ShowAsync();
                        if (result == ContentDialogResult.Primary)
                        {
                            var response = await MainPage.MyClient.DeleteUserAccount();
                            if (response == System.Net.HttpStatusCode.OK)
                            {
                                MainPage.currentUser = new MyUser();
                                MainPage.currentUser.name = "Gast";
                                MainPage.currentUser.email = "gast@beerdude.de";
                                APIConstant.MyClientAuthenticationHeader = null;
                                MainPage.MyClient.DefaultRequestHeaders.Authorization = null;
                                APIConstant.userIsLoggedIn = false;
                                MainPage.ShowGeneralMessage("Account wurde erfolgreich gelöscht!", MainPage.NotifyType.StatusMessage);
                                this.Frame.Navigate(typeof(Settings));
                            }
                            else
                            {
                                MainPage.ShowGeneralMessage(MainPage.MyClient.HandleHttpCode(response), MainPage.NotifyType.ErrorMessage);
                            }
                        }
                    }
                    catch (Exception Error)
                    {
                        MainPage.SendBugReport(Error, this);
                    }

                    break;
                case "UpdateProfileItem":
                    this.Frame.Navigate(typeof(UpdateUser));
                    break;
                case "ChangePasswordItem":
                    this.Frame.Navigate(typeof(PasswordChange));
                    break;
                default:
                    break;
            }
        }
    }
}
