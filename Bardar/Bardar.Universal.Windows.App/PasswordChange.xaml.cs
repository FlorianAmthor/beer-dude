﻿using System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace Bardar.Universal.Windows.App
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class PasswordChange : Page
    {
        private string forgottenPasswordEmail;
        public PasswordChange()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            }
            else
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            }
            if (e.Parameter != null)
            {
                forgottenPasswordEmail = e.Parameter.ToString();
            }
            ChangePasswordInfo.Visibility = Visibility;
            ConfirmationCodeTextBox.Visibility = Visibility.Visible;
            ChangePasswordInfo.Visibility = Visibility.Visible;
            if (APIConstant.userIsLoggedIn)
            {
                ChangePasswordInfo.Visibility = Visibility.Collapsed;
                ConfirmationCodeTextBox.Visibility = Visibility.Collapsed;
                ChangePasswordInfo.Visibility = Visibility.Collapsed;
            }
        }

        private async void PasswordChangeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (NewPasswordBox.Password == "" || NewPasswordConfirmBox.Password == "")
                {
                    MainPage.ShowGeneralMessage("Passwort darf nicht leer sein!", MainPage.NotifyType.ErrorMessage);
                }
                else if (NewPasswordBox.Password != NewPasswordConfirmBox.Password)
                {
                    MainPage.ShowGeneralMessage("Die Passwörter stimmen nicht überein!", MainPage.NotifyType.ErrorMessage);
                }
                else if (true)
                {
                    if (APIConstant.userIsLoggedIn)
                    {
                        var statusCode = await MainPage.MyClient.ChangeUserPassword(NewPasswordBox.Password);

                        if (statusCode == System.Net.HttpStatusCode.OK)
                        {
                            MainPage.ShowGeneralMessage("Passwort wurde erfolgreich geändert!", MainPage.NotifyType.StatusMessage);
                        }
                        else
                        {
                            MainPage.ShowGeneralMessage(MainPage.MyClient.HandleHttpCode(statusCode), MainPage.NotifyType.ErrorMessage);
                        }
                    }
                    else
                    {
                        if (ConfirmationCodeTextBox.Text == "")
                        {
                            MainPage.ShowGeneralMessage("Es muss ein Bestätigungscode angegeben werden!", MainPage.NotifyType.StatusMessage);
                        }
                        else
                        {
                            var statusCode = await MainPage.MyClient.ChangeForgottenUserPassword(forgottenPasswordEmail, NewPasswordBox.Password, ConfirmationCodeTextBox.Text);

                            if (statusCode == System.Net.HttpStatusCode.OK)
                            {
                                MainPage.ShowGeneralMessage("Passwort wurde erfolgreich geändert!", MainPage.NotifyType.StatusMessage);
                            }
                            else
                            {
                                MainPage.ShowGeneralMessage(MainPage.MyClient.HandleHttpCode(statusCode), MainPage.NotifyType.ErrorMessage);
                            }
                        }
                    }
                    this.Frame.Navigate(typeof(Settings));
                }
            }
            catch (Exception Error)
            {
                MainPage.SendBugReport(Error, this);
            }
        }
    }
}
