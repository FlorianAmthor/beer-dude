﻿using System;
using System.Text.RegularExpressions;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace Bardar.Universal.Windows.App
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class Register : Page
    {
        public Register()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            }
            else
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            }
        }

        private async void FinishRegisterButton_Click(object sender, RoutedEventArgs e)
        {
            if (EmailRegisterTextBox.Text == "" || PasswordRegisterBox.Password == "" || PasswordRegisterConfirmBox.Password == "")
            {
                MainPage.ShowGeneralMessage("Email oder Passwort darf nicht leer sein!", MainPage.NotifyType.ErrorMessage);
            }
            else
            {

                bool isEmail = Regex.IsMatch(EmailRegisterTextBox.Text, @"\A(?:[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?)\Z");

                if (PasswordRegisterBox.Password == PasswordRegisterConfirmBox.Password && isEmail)
                {
                    try
                    {
                        if (EmailRegisterTextBox.Text != "")
                        {
                            var statusCode = await MainPage.MyClient.RegisterUser(EmailRegisterTextBox.Text, PasswordRegisterBox.Password);
                            if (statusCode == System.Net.HttpStatusCode.OK)
                            {
                                MainPage.ShowGeneralMessage("Um deine Anmeldung abzuschließen, klicke bitte auf Verifizierungslink in deiner Email!", MainPage.NotifyType.StatusMessage);
                            }
                            else
                            {
                                MainPage.ShowGeneralMessage(MainPage.MyClient.HandleHttpCode(statusCode), MainPage.NotifyType.ErrorMessage);
                            }
                            this.Frame.Navigate(typeof(Settings));
                        }
                    }
                    catch (Exception Error)
                    {
                        MainPage.SendBugReport(Error, this);
                    }
                }
                else
                {
                    if (!isEmail)
                    {
                        MainPage.ShowGeneralMessage("Keine gültige Email Adresse!", MainPage.NotifyType.ErrorMessage);
                    }
                    else
                    {
                        MainPage.ShowGeneralMessage("Passwörter stimmen nicht überein!", MainPage.NotifyType.ErrorMessage);
                    }
                }
            }
        }
    }
}
