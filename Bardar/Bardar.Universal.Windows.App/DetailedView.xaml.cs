﻿using System;
using System.Collections.Generic;
using Windows.System;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace Bardar.Universal.Windows.App
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    /// 

    public sealed partial class DetailedView : Page
    {
        private MyBar _currentBar;
        private Uri _mapsUri;
        public DetailedView()
        {
            this.InitializeComponent();
            _currentBar = new MyBar();
        }


        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            }
            else
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            }

            try
            {

                List<MyEvent> eventObjList = new List<MyEvent>();
                var barId = (string)e.Parameter;

                _currentBar = await MainPage.MyClient.GetMyBarRequest(barId);
                eventObjList = await MainPage.MyClient.GetMyEventListRequestForBar(barId);

                foreach (var item in MainPage.currentUser.favourites)
                {
                    if (item._id == _currentBar._id)
                    {
                        FavoriteIconBottom.Foreground = new SolidColorBrush(Colors.Yellow);
                    }
                }

                BarNameTextBlock.Text = _currentBar.name;
                BarAddressTextBlock.Text = _currentBar.contact.street + " " + _currentBar.contact.number;
                BarZIPTextBlock.Text = _currentBar.contact.ZIP + " " + _currentBar.contact.city;
                BarRatingTextBlock.Text = _currentBar.rating.ToString() + "%";
                BarBitMapImage.UriSource = _currentBar.imageUri;

                foreach (var menuBeverage in _currentBar.menu)
                {
                    ListBoxItem beverage = new ListBoxItem();
                    beverage.Content = menuBeverage.name + ": " + menuBeverage.price + "\u20AC";
                    DetailedViewPreisListBox.Items.Add(beverage);
                }

                foreach (var bEvent in eventObjList)
                {
                    ListBoxItem barEvent = new ListBoxItem();
                    barEvent.Content = bEvent.name + "\nInfo: " + bEvent.description;
                    barEvent.Tag = bEvent._id;
                    barEvent.Tapped += BarEvent_Tapped;
                    DetailedViewEventListBox.Items.Add(barEvent);
                }
            }
            catch (Exception Error)
            {
                MainPage.SendBugReport(Error, this);
            }
        }

        private void BarEvent_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var bEventListBoxItem = sender as ListBoxItem;
            this.Frame.Navigate(typeof(DetailedViewEvent), bEventListBoxItem.Tag.ToString());
        }

        private void FavoriteIcon_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var c = FavoriteIconBottom.Foreground as SolidColorBrush;
            if (c.Color.Equals(Colors.White))
            {
                FavoriteIconBottom.Foreground = new SolidColorBrush(Colors.Yellow);
                MainPage.currentUser.favourites.Add(_currentBar);
                CustomSplashScreen.splashPage.favList.Add(_currentBar._id);
            }
            else
            {
                FavoriteIconBottom.Foreground = new SolidColorBrush(Colors.White);
                for (int i = 0; i < MainPage.currentUser.favourites.Count; i++)
                {
                    if (MainPage.currentUser.favourites[i]._id == _currentBar._id)
                    {
                        MainPage.currentUser.favourites.Remove(MainPage.currentUser.favourites[i]);
                    }
                }
                CustomSplashScreen.splashPage.favList.Remove(_currentBar._id);
            }

        }

        private async void AddItemDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (AddItemNameContenDialogTextBox.Text =="" || AddItemPriceContenDialogTextBox.Text == "")
            {
                MainPage.ShowGeneralMessage("Es muss ein Getränkname und ein Preis eingegeben werden!", MainPage.NotifyType.ErrorMessage);
            }
            else
            {
                try
                {
                    MyBeverage beverage = new MyBeverage();
                    beverage.name = AddItemNameContenDialogTextBox.Text;
                    beverage.price = float.Parse(AddItemPriceContenDialogTextBox.Text);
                    var result = await MainPage.MyClient.UpdateBarMenu(_currentBar._id, beverage);
                    if (result == System.Net.HttpStatusCode.OK)
                    {
                        MainPage.ShowGeneralMessage("Bewertung erfolgreich abgegeben!", MainPage.NotifyType.StatusMessage);
                        this.Frame.Navigate(typeof(DetailedView), _currentBar._id);
                    }
                }
                catch (Exception Error)
                {
                    MainPage.SendBugReport(Error, this);
                }
            }
        }

        private async void ItemAddButton_Click(object sender, RoutedEventArgs e)
        {
            if (!APIConstant.userIsLoggedIn)
            {
                MainPage.ShowGeneralMessage("Für diese Aktion wird ein Account benötigt!", MainPage.NotifyType.ErrorMessage);
            }
            else
            {
                var result = await AddItemDialog.ShowAsync();
            }
        }

        private async void BarRateButton_Click(object sender, RoutedEventArgs e)
        {
            if (APIConstant.userIsLoggedIn)
            {
                try
                {
                    RateBarSlider.Value = _currentBar.rating;
                    var result = await RateBarDialog.ShowAsync();
                    if (result == ContentDialogResult.Primary)
                    {
                        var keyValueStatusCodeInt = await MainPage.MyClient.RateBar(_currentBar._id, (int)RateBarSlider.Value);
                        if (keyValueStatusCodeInt.Key == System.Net.HttpStatusCode.OK)
                        {
                            _currentBar.rating = keyValueStatusCodeInt.Value;
                            BarRatingTextBlock.Text = _currentBar.rating.ToString() + "%";
                        }
                        else
                        {
                            MainPage.ShowGeneralMessage("Dafür musst du eingeloggt sein!", MainPage.NotifyType.ErrorMessage);
                        }
                    }
                }
                catch (Exception Error)
                {
                    MainPage.SendBugReport(Error, this);
                }
            }
            else
            {
                MainPage.ShowGeneralMessage("Dazu musst du eingeloggt sein!", MainPage.NotifyType.ErrorMessage);
            }
        }

        async private void NaviationButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _mapsUri = new Uri(@"bingmaps:?rtp=adr." + _currentBar.contact.street + " " + _currentBar.contact.number + ", " + _currentBar.contact.city + ", " + _currentBar.contact.ZIP);
                var launcherOptions = new LauncherOptions();
                launcherOptions.TargetApplicationPackageFamilyName = "Microsoft.WindowsMaps_8wekyb3d8bbwe";
                var success = await Launcher.LaunchUriAsync(_mapsUri, launcherOptions);
            }
            catch (Exception Error)
            {
                MainPage.SendBugReport(Error, this);
            }
        }

        private void AddItemDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            AddItemPriceContenDialogTextBox.Text = "";
            AddItemNameContenDialogTextBox.Text = "";
        }
    }
}
